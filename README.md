## Домашние задания OTUS «Разработчик Golang»
1. [«Hello now»](./hw-1)
2. [«Распаковка строки»](./hw-2)
3. [«Частотный анализ»](./hw-3)
4. [«Двусвязный список»](./hw-4)
5. [«Параллельное выполнение](./hw-5)
6. [«Утилита для копирования файлов»](./hw-6)
7. [«Утилита envdir»](./hw-7)
8. [«Заготовка для микросервиса Календарь»](./hw-8)
9. [«Заготовка для микросервиса Календарь» (logger, config, web server)](./hw-9)
10. [«Реализовать примитивный telnet клиент»](./hw-10)
11. [«HTTP интерфейс»](./hw-11)
12. [«GRPC сервис»](./hw-12)