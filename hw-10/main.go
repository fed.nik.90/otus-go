package main

import (
	"bufio"
	"context"
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"os/signal"
	"syscall"
	"time"
)

var (
	timeout int
	host    string
	port    string
)

func readRoutine(ctx context.Context, conn io.Reader, cancel context.CancelFunc, stop chan<- os.Signal) {
	scanner := bufio.NewScanner(conn)

READ:
	for {
		select {
		case <-ctx.Done():
			break READ
		default:
			if !scanner.Scan() {
				log.Printf("CANNOT SCAN")
				stop <- os.Signal(syscall.SIGTERM)
				cancel()
				break READ
			}
			text := scanner.Text()
			log.Printf("From server: %s", text)
		}
	}
	log.Printf("Finished readRoutine")
}

func writeRoutine(ctx context.Context, conn io.Writer, stop chan<- os.Signal) {
	scanner := bufio.NewScanner(os.Stdin)

WRITE:
	for {
		select {
		case <-ctx.Done():
			break WRITE
		default:
			if !scanner.Scan() {
				stop <- os.Signal(syscall.SIGTERM)
				break WRITE
			}
			text := scanner.Text()
			log.Printf("To server %v\n", text)
			_, err := conn.Write([]byte(fmt.Sprintf("%s\n", text)))
			if err != nil {
				log.Fatal(err)
			}
		}
	}
	log.Printf("Finished writeRoutine")
}

func init() {
	flag.IntVar(&timeout, "timeout", 60, "timeout")
	flag.StringVar(&host, "host", "127.0.0.1", "host")
	flag.StringVar(&port, "port", "8008", "port")
}

func main() {
	dialer := &net.Dialer{}
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(timeout)*time.Second)
	addr := host + ":" + port
	conn, err := dialer.DialContext(ctx, "tcp", addr)
	if err != nil {
		log.Fatal(err)
	}
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		readRoutine(ctx, conn, cancel, interrupt)
	}()

	go func() {
		writeRoutine(ctx, conn, interrupt)
	}()

	<-interrupt

	err = conn.Close()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Connection closed")
}
