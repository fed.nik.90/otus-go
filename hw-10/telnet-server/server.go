package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"syscall"
)

func serverHandler(conn net.Conn, stop chan<- os.Signal) {
	fmt.Printf("Local Addr '%s'\nRemote Addr '%s'\n", conn.LocalAddr(), conn.RemoteAddr())

	scanner := bufio.NewScanner(conn)
LOOP:
	for {
		if scanner.Scan() {
			text := scanner.Text()
			log.Printf("RECEIVED: %s", text)
			if text == "quit" || text == "exit" {
				stop <- os.Signal(syscall.SIGTERM)
				break LOOP
			}
			_, err := conn.Write([]byte(fmt.Sprintf("I have received '%s'\n", text)))
			if err != nil {
				log.Fatal(err)
			}
		} else {
			stop <- os.Signal(syscall.SIGTERM)
			break LOOP
		}
	}

	err := conn.Close()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Scan stop")
}

func main() {
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, syscall.SIGINT, syscall.SIGTERM)
	l, err := net.Listen("tcp", "0.0.0.0:8008")
	if err != nil {
		log.Fatalf("Cannot listen: %v", err)
	}
	defer l.Close()

LISTEN:
	for {
		conn, err := l.Accept()
		if err != nil {
			log.Fatal(err)
		}
		go serverHandler(conn, interrupt)
		select {
		case <-interrupt:
			break LISTEN
		}
	}
	fmt.Println("Listen stop")
}
