package main

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestRun(t *testing.T) {
	t.Run("TestRunSuccess", func(t *testing.T) {
		ok := true
		tasks := make([]Task, 0, numWorks)
		for i := 0; i < numWorks; i++ {
			tasks = append(tasks, func() error {
				return nil
			})
		}
		err := Run(tasks, numWorks, numErrors)
		if err != nil {
			ok = false
		}
		require.Equal(t, true, ok)
	})
	t.Run("TestRunUnEqual", func(t *testing.T) {
		ok := true
		tasks := make([]Task, 0, numWorks)
		for i := 0; i < numWorks; i++ {
			tasks = append(tasks, func() error {
				return ErrLimit
			})
		}
		err := Run(tasks, numWorks, numErrors)
		if err != nil {
			ok = false
		}
		require.NotEqual(t, true, ok)
	})
}
