package main

import (
	"errors"
	"fmt"
	"math/rand"
	"sync"
	"time"
)

const (
	numWorks  = 10
	numErrors = 3
)

var ErrLimit = errors.New("errors limit exceeded")

type Task func() error

func randInt(min int, max int) int {
	return min + rand.Intn(max-min)
}

func getInt() int {
	rand.Seed(time.Now().UTC().UnixNano())
	return randInt(1, 5)
}

func checkInt() error {
	if getInt() < 3 {
		return ErrLimit
	}
	return nil
}

func main() {
	tasks := make([]Task, 0, numWorks)
	for i := 0; i < numWorks; i++ {
		tasks = append(tasks, checkInt)
	}
	fmt.Println("Start")
	err := Run(tasks, numWorks, numErrors)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("Done")
}

func Run(tasks []Task, n int, m int) error {
	errLimit := m
	taskCh := make(chan Task, len(tasks))
	wg := sync.WaitGroup{}
	mu := sync.Mutex{}
	wg.Add(n)

	for i := 0; i < n; i++ {
		go func() {
			defer wg.Done()
			worker(taskCh, &mu, &errLimit)
		}()
	}
	for _, t := range tasks {
		taskCh <- t
	}
	close(taskCh)
	wg.Wait()

	if errLimit < 0 {
		return ErrLimit
	}
	return nil
}

func worker(taskCh <-chan Task, mu sync.Locker, errLimit *int) {
	stop := false
	for t := range taskCh {
		err := t()
		mu.Lock()
		if *errLimit < 1 {
			stop = true
		}
		if err != nil {
			*errLimit--
		}
		mu.Unlock()
		if stop {
			return
		}
	}
}
