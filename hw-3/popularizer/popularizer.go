package popularizer

import (
	"sort"
	"strings"
)

const (
	wordCount = 10
)

type word struct {
	word  string
	count int
}

func Popularize(str string) []string {
	s := getSplitWords(str)
	m := getMapWords(s)
	sl := convertMapToSlice(m)
	sortSliceWords(sl)
	return getPopularizedWords(sl)
}

func getSplitWords(str string) []string {
	return strings.Split(str, " ")
}

func getMapWords(s []string) map[string]word {
	m := make(map[string]word, len(s))
	for _, v := range s {
		if val, ok := m[v]; ok {
			val.count++
			m[v] = val
		} else {
			val.word = v
			val.count = 1
			m[v] = val
		}
	}
	return m
}

func convertMapToSlice(m map[string]word) []word {
	sl := make([]word, 0, len(m))
	for _, v := range m {
		sl = append(sl, v)
	}
	return sl
}

func sortSliceWords(sl []word) {
	sort.Slice(sl, func(i, j int) bool {
		return sl[i].count > sl[j].count
	})
}

func getPopularizedWords(sl []word) []string {
	var res []string
	var lensl int
	if len(sl) >= wordCount {
		lensl = wordCount - 1
	} else {
		lensl = len(sl) - 1
	}
	for i := 0; i <= lensl; i++ {
		res = append(res, sl[i].word)
	}

	return res
}
