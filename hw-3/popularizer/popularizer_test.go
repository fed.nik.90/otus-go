package popularizer

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestGetSplitWords(t *testing.T) {
	t.Run("TestGetSplitWordsSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			in  string
			out []string
		}{
			{
				in:  "name file test",
				out: []string{"name", "file", "test"},
			},
			{
				in:  "name test",
				out: []string{"name", "test"},
			},
			{
				in:  "name file 123",
				out: []string{"name", "file", "123"},
			},
			{
				in:  "",
				out: []string{""},
			},
		}
		for _, v := range casesSuccess {
			t.Run("HW-3 if split words from text to slice  no err", func(t *testing.T) {
				res := getSplitWords(v.in)
				require.Equal(t, v.out, res)
			})
		}
	})

	t.Run("GetSplitWordsUnEqual", func(t *testing.T) {
		casesNotEqual := []struct {
			in  string
			out []string
		}{
			{
				in:  "name file test",
				out: []string{" "},
			},
			{
				in:  "name file test",
				out: []string{"name", "test"},
			},
			{
				in:  " name file test",
				out: []string{" name", "file", "test"},
			},
		}
		for _, v := range casesNotEqual {
			t.Run("if split words from text to slice  no equal no err", func(t *testing.T) {
				res := getSplitWords(v.in)
				require.NotEqual(t, v.out, res)
			})
		}
	})
}

func TestGetMapWords(t *testing.T) {
	t.Run("TestGetMapWordsSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			in  []string
			out map[string]word
		}{
			{
				in: []string{"name", "file", "test"},
				out: map[string]word{
					"name": {
						word:  "name",
						count: 1,
					},
					"file": {
						word:  "file",
						count: 1,
					},
					"test": {
						word:  "test",
						count: 1,
					},
				},
			},
			{
				in: []string{"name", "name", "test"},
				out: map[string]word{
					"name": {
						word:  "name",
						count: 2,
					},
					"test": {
						word:  "test",
						count: 1,
					},
				},
			},
			{
				in: []string{"name", "name", "name"},
				out: map[string]word{
					"name": {
						word:  "name",
						count: 3,
					},
				},
			},
		}
		for _, v := range casesSuccess {
			t.Run("if created words dictionary  no err", func(t *testing.T) {
				res := getMapWords(v.in)
				require.Equal(t, v.out, res)
			})
		}
	})

	t.Run("TestGetMapWordsUnEqual", func(t *testing.T) {
		casesNotEqual := []struct {
			in  []string
			out map[string]word
		}{
			{
				in: []string{"name", "name", "name"},
				out: map[string]word{
					"name": {
						word:  "name",
						count: 1,
					},
				},
			},
			{
				in: []string{"name", "test", "name"},
				out: map[string]word{
					"name": {
						word:  "name",
						count: 1,
					},
					"test": {
						word:  "test",
						count: 1,
					},
				},
			},
			{
				in: []string{"name", "test", "name"},
				out: map[string]word{
					"name": {
						word:  "name",
						count: 1,
					},
					"test": {
						word:  "test",
						count: 2,
					},
				},
			},
		}
		for _, v := range casesNotEqual {
			t.Run("if created words dictionary  no equal no err", func(t *testing.T) {
				res := getMapWords(v.in)
				require.NotEqual(t, v.out, res)
			})
		}
	})
}

func TestConvertMapToSlice(t *testing.T) {
	t.Run("TestConvertMapToSliceSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			in  map[string]word
			out []word
		}{
			{
				in: map[string]word{
					"name": {
						word:  "name",
						count: 1,
					},
					"file": {
						word:  "file",
						count: 1,
					},
					"test": {
						word:  "test",
						count: 1,
					},
				},
				out: []word{
					{"name", 1},
					{"file", 1},
					{"test", 1},
				},
			},
		}
		for _, v := range casesSuccess {
			t.Run("if converted map to slice  no err", func(t *testing.T) {
				res := convertMapToSlice(v.in)
				for _, rv := range res {
					var ok bool
					for _, outV := range v.out {
						if rv == outV {
							ok = true
							continue
						}
					}
					require.Equal(t, true, ok)
				}
			})
		}
	})

	t.Run("TestConvertMapToSliceUnEqual", func(t *testing.T) {
		casesNotEqual := []struct {
			in  map[string]word
			out []word
		}{
			{
				in: map[string]word{
					"name": {
						word:  "name",
						count: 1,
					},
					"file": {
						word:  "file",
						count: 1,
					},
					"test": {
						word:  "test",
						count: 1,
					},
				},
				out: []word{
					{"name", 1},
					{"file", 2},
					{"test", 3},
				},
			},
		}
		for _, v := range casesNotEqual {
			t.Run("if converted map to slice  no equal no err", func(t *testing.T) {
				res := convertMapToSlice(v.in)
				var sl []string
				var ok bool
				for _, rv := range res {
					for _, outV := range v.out {
						if rv.word == outV.word && rv.count != outV.count {
							sl = append(sl, rv.word)
							continue
						}
					}
				}
				if len(sl) > 0 {
					ok = true
				}
				require.NotEqual(t, false, ok)
			})
		}
	})
}

func TestGetPopularizedWords(t *testing.T) {
	t.Run("TestGetPopularizedWordsSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			in  []word
			out []string
		}{
			{
				in: []word{
					{
						word:  "name",
						count: 3,
					},
					{
						word:  "file",
						count: 2,
					},
					{
						word:  "test",
						count: 1,
					},
				},
				out: []string{
					"name", "file", "test",
				},
			},
			{
				in: []word{
					{
						word:  "name",
						count: 3,
					},
					{
						word:  "file",
						count: 2,
					},
					{
						word:  "test",
						count: 1,
					},
					{
						word:  "proba",
						count: 3,
					},
					{
						word:  "sentence",
						count: 2,
					},
					{
						word:  "paragraph",
						count: 1,
					},
					{
						word:  "text",
						count: 3,
					},
					{
						word:  "dictionary",
						count: 2,
					},
					{
						word:  "notebook",
						count: 1,
					},
					{
						word:  "computer",
						count: 3,
					},
					{
						word:  "apple",
						count: 2,
					},
					{
						word:  "android",
						count: 1,
					},
				},
				out: []string{
					"name", "file", "test", "proba", "sentence", "paragraph", "text", "dictionary", "notebook", "computer",
				},
			},
			{
				in: []word{
					{
						word:  "file",
						count: 2,
					},
					{
						word:  "test",
						count: 3,
					},
					{
						word:  "proba",
						count: 1,
					},
				},
				out: []string{
					"file", "test", "proba",
				},
			},
			{
				in: []word{
					{
						word:  "file",
						count: 2,
					},
				},
				out: []string{
					"file",
				},
			},
		}
		for _, v := range casesSuccess {
			t.Run("if get slice of popularized words from slice dictionary  no err", func(t *testing.T) {
				res := getPopularizedWords(v.in)
				require.Equal(t, v.out, res)
			})
		}
	})

	t.Run("TestGetPopularizedWordsUnEqual", func(t *testing.T) {
		casesNotEqual := []struct {
			in  []word
			out []string
		}{
			{
				in:  []word{},
				out: []string{""},
			},
			{
				in: []word{
					{
						word:  "test",
						count: 3,
					},
					{
						word:  "proba",
						count: 1,
					},
				},
				out: []string{
					"", "test", "proba",
				},
			},
			{
				in: []word{
					{
						word:  "file",
						count: 2,
					},
				},
				out: []string{
					"test", "test", "test",
				},
			},
		}
		for _, v := range casesNotEqual {
			t.Run("if get slice of popularized words from slice dictionary  no equal no err", func(t *testing.T) {
				res := getPopularizedWords(v.in)
				require.NotEqual(t, v.out, res)
			})
		}
	})
}

func TestPopularize(t *testing.T) {
	t.Run("TestPopularizeSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			in  string
			out []string
		}{
			{
				in:  "",
				out: []string{""},
			},
			{
				in:  "test",
				out: []string{"test"},
			},
			{
				in:  "name sentence file file file name",
				out: []string{"file", "name", "sentence"},
			},
			{
				in:  "name name name name file file",
				out: []string{"name", "file"},
			},
		}
		for _, v := range casesSuccess {
			t.Run("if get slice of popular words  no err", func(t *testing.T) {
				res := Popularize(v.in)
				require.Equal(t, v.out, res)
			})
		}
	})

	t.Run("TestPopularizeUnEqual", func(t *testing.T) {
		casesNotEqual := []struct {
			in  string
			out []string
		}{
			{
				in:  "",
				out: []string{"", ""},
			},
			{
				in:  "test",
				out: []string{"name"},
			},
			{
				in:  "name sentence sentence file file filepath file name",
				out: []string{"name", "name", "sentence", "filepath"},
			},
			{
				in:  "name name name name file file",
				out: []string{"file", "name"},
			},
		}
		for _, v := range casesNotEqual {
			t.Run("if get slice of popular words  no equal no err", func(t *testing.T) {
				res := Popularize(v.in)
				require.NotEqual(t, v.out, res)
			})
		}
	})
}
