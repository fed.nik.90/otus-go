package main

import (
	"fmt"

	"gitlab.com/fed.nik.90/otus-go/hw-3/popularizer"
)

func main() {
	str := "name sentence file file filepath file name"
	res := popularizer.Popularize(str)
	fmt.Println(res)
}
