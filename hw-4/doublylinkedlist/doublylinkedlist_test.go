package doublylinkedlist

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestLen(t *testing.T) {
	t.Run("TestLenSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			in  []*Item
			out int
		}{
			{
				in:  []*Item{{1}, {2}},
				out: 2,
			},
			{
				in:  []*Item{},
				out: 0,
			},
			{
				in:  []*Item{{"test"}, {123}, {[]int{}}},
				out: 3,
			},
		}
		t.Run("get list length  success", func(t *testing.T) {
			for _, v := range casesSuccess {
				l := List{}
				l.items = append(l.items, v.in...)
				res := l.Len()
				require.Equal(t, v.out, res)
			}
		})
	})
	t.Run("TestLenUnEqual", func(t *testing.T) {
		casesNotEqual := []struct {
			in  []*Item
			out int
		}{
			{
				in:  []*Item{{1}, {2}},
				out: 0,
			},
			{
				in:  []*Item{},
				out: 1,
			},
			{
				in:  []*Item{{"test"}, {123}, {[]int{}}},
				out: 1,
			},
		}
		t.Run("get list length  notequal", func(t *testing.T) {
			for _, v := range casesNotEqual {
				l := List{}
				l.items = append(l.items, v.in...)
				res := l.Len()
				require.NotEqual(t, v.out, res)
			}
		})
	})
}

func TestGetItem(t *testing.T) {
	t.Run("TestGetItemSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			in  Item
			out interface{}
		}{
			{
				in:  Item{13},
				out: 13,
			},
			{
				in:  Item{"test"},
				out: "test",
			},
			{
				in:  Item{},
				out: nil,
			},
		}
		t.Run("get item  success", func(t *testing.T) {
			for _, v := range casesSuccess {
				i := Item{}
				i.value = v.in.value
				res := i.GetItem()
				require.Equal(t, v.out, res)
			}
		})
	})
	t.Run("TestGetItemUnEqual", func(t *testing.T) {
		casesNotEqual := []struct {
			in  Item
			out interface{}
		}{
			{
				in:  Item{13},
				out: 1,
			},
			{
				in:  Item{"test"},
				out: 0,
			},
			{
				in:  Item{},
				out: "",
			},
		}
		t.Run("get item  not equal", func(t *testing.T) {
			for _, v := range casesNotEqual {
				i := Item{}
				i.value = v.in.value
				res := i.GetItem()
				require.NotEqual(t, v.out, res)
			}
		})
	})
}

func TestFirst(t *testing.T) {
	t.Run("TestFirstSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			in  []*Item
			out *Item
		}{
			{
				in:  []*Item{{1}, {2}},
				out: &Item{1},
			},
			{
				in:  []*Item{{}},
				out: &Item{nil},
			},
			{
				in:  []*Item{{"test"}, {123}, {[]int{}}},
				out: &Item{"test"},
			},
		}
		t.Run("get first item  success", func(t *testing.T) {
			for _, v := range casesSuccess {
				l := List{}
				l.items = append(l.items, v.in...)
				res := l.First()
				require.Equal(t, v.out, res)
			}
		})
	})
	t.Run("TestFirstUnEqual", func(t *testing.T) {
		casesNotEqual := []struct {
			in  []*Item
			out *Item
		}{
			{
				in:  []*Item{{1}, {2}},
				out: &Item{2},
			},
			{
				in:  []*Item{{}},
				out: &Item{0},
			},
			{
				in:  []*Item{{"test"}, {123}, {[]int{}}},
				out: &Item{1},
			},
		}
		t.Run("get first item  not equal", func(t *testing.T) {
			for _, v := range casesNotEqual {
				l := List{}
				l.items = append(l.items, v.in...)
				res := l.First()
				require.NotEqual(t, v.out, res)
			}
		})
	})
}

func TestLast(t *testing.T) {
	t.Run("TestLastSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			in  []*Item
			out *Item
		}{
			{
				in:  []*Item{{1}, {2}},
				out: &Item{2},
			},
			{
				in:  []*Item{{}},
				out: &Item{nil},
			},
			{
				in:  []*Item{{"test"}, {123}, {[]int{}}},
				out: &Item{[]int{}},
			},
		}
		t.Run("get last item  success", func(t *testing.T) {
			for _, v := range casesSuccess {
				l := List{}
				l.items = append(l.items, v.in...)
				res := l.Last()
				require.Equal(t, v.out, res)
			}
		})
	})
	t.Run("TestLastUnEqual", func(t *testing.T) {
		casesNotEqual := []struct {
			in  []*Item
			out *Item
		}{
			{
				in:  []*Item{{1}, {2}},
				out: &Item{1},
			},
			{
				in:  []*Item{{}},
				out: &Item{1},
			},
			{
				in:  []*Item{{"test"}, {123}, {[]int{}}},
				out: &Item{"test"},
			},
		}
		t.Run("get last item  not equal", func(t *testing.T) {
			for _, v := range casesNotEqual {
				l := List{}
				l.items = append(l.items, v.in...)
				res := l.Last()
				require.NotEqual(t, v.out, res)
			}
		})
	})
}

func TestPushFront(t *testing.T) {
	t.Run("TestPushFrontSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			in  []*Item
			val interface{}
			out *Item
		}{
			{
				in:  []*Item{{1}, {2}},
				val: 3,
				out: &Item{3},
			},
			{
				in:  []*Item{{1}, {2}, {"example"}},
				val: "test",
				out: &Item{"test"},
			},
			{
				in:  []*Item{{1}, {2}, {"example"}},
				val: []int{},
				out: &Item{[]int{}},
			},
		}
		t.Run("push item to front  success", func(t *testing.T) {
			for _, v := range casesSuccess {
				l := List{}
				l.items = append(l.items, v.in...)
				l.PushFront(v.val)
				res := l.First()
				require.Equal(t, v.out, res)
			}
		})
	})
	t.Run("TestPushFrontUnEqual", func(t *testing.T) {
		casesNotEqual := []struct {
			in  []*Item
			val interface{}
			out *Item
		}{
			{
				in:  []*Item{{1}, {2}},
				val: 3,
				out: &Item{2},
			},
			{
				in:  []*Item{{1}, {2}, {"example"}},
				val: "test",
				out: &Item{nil},
			},
			{
				in:  []*Item{{1}, {2}, {"example"}},
				val: []int{},
				out: &Item{1},
			},
		}
		t.Run("push item to front  not equal", func(t *testing.T) {
			for _, v := range casesNotEqual {
				l := List{}
				l.items = append(l.items, v.in...)
				l.PushFront(v.val)
				res := l.First()
				require.NotEqual(t, v.out, res)
			}
		})
	})
}

func TestPushBack(t *testing.T) {
	t.Run("TestPushBackSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			in  []*Item
			val interface{}
			out *Item
		}{
			{
				in:  []*Item{{1}, {2}},
				val: 3,
				out: &Item{3},
			},
			{
				in:  []*Item{{1}, {2}, {"example"}},
				val: "test",
				out: &Item{"test"},
			},
			{
				in:  []*Item{{1}, {2}, {"example"}},
				val: []int{},
				out: &Item{[]int{}},
			},
		}
		t.Run("push item to back  success", func(t *testing.T) {
			for _, v := range casesSuccess {
				l := List{}
				l.items = append(l.items, v.in...)
				l.PushBack(v.val)
				res := l.Last()
				require.Equal(t, v.out, res)
			}
		})
	})
	t.Run("TestPushBackUnEqual", func(t *testing.T) {
		casesNotEqual := []struct {
			in  []*Item
			val interface{}
			out *Item
		}{
			{
				in:  []*Item{{1}, {2}},
				val: 3,
				out: &Item{2},
			},
			{
				in:  []*Item{{1}, {2}, {"example"}},
				val: "test",
				out: &Item{nil},
			},
			{
				in:  []*Item{{1}, {2}, {"example"}},
				val: []int{},
				out: &Item{1},
			},
		}
		t.Run("push item to back  not equal", func(t *testing.T) {
			for _, v := range casesNotEqual {
				l := List{}
				l.items = append(l.items, v.in...)
				l.PushBack(v.val)
				res := l.Last()
				require.NotEqual(t, v.out, res)
			}
		})
	})
}

func TestRemove(t *testing.T) {
	t.Run("TestRemoveSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			in  []*Item
			pos int
			out *Item
		}{
			{
				in:  []*Item{{1}, {2}, {3}, {4}},
				pos: 2,
				out: &Item{3},
			},
			{
				in:  []*Item{{1}, {2}, {"example"}},
				pos: 2,
				out: &Item{"example"},
			},
			{
				in:  []*Item{{1}, {2}, {"example"}},
				pos: 1,
				out: &Item{2},
			},
			{
				in:  []*Item{{1}, {2}, {"example"}},
				pos: 3,
				out: nil,
			},
		}
		t.Run("remove item  success", func(t *testing.T) {
			for _, v := range casesSuccess {
				l := List{}
				l.items = append(l.items, v.in...)
				res := l.Remove(v.pos)
				require.Equal(t, v.out, res)
			}
		})
	})
	t.Run("TestRemoveUnEqual", func(t *testing.T) {
		casesNotEqual := []struct {
			in  []*Item
			pos int
			out *Item
		}{
			{
				in:  []*Item{{1}, {2}},
				pos: 1,
				out: &Item{1},
			},
			{
				in:  []*Item{{1}, {2}, {"example"}},
				pos: 2,
				out: &Item{2},
			},
			{
				in:  []*Item{{1}, {2}, {"example"}},
				pos: 1,
				out: &Item{1},
			},
		}
		t.Run("remove item  not equal", func(t *testing.T) {
			for _, v := range casesNotEqual {
				l := List{}
				l.items = append(l.items, v.in...)
				res := l.Remove(v.pos)
				require.NotEqual(t, v.out, res)
			}
		})
	})
}

func TestIssetItemPosition(t *testing.T) {
	t.Run("TestIssetItemPositionSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			in  []*Item
			pos int
			out bool
		}{
			{
				in:  []*Item{{1}, {2}, {3}, {4}},
				pos: 2,
				out: true,
			},
			{
				in:  []*Item{{1}, {2}, {"example"}},
				pos: 3,
				out: false,
			},
			{
				in:  []*Item{{1}, {2}, {"example"}},
				pos: 0,
				out: true,
			},
		}
		t.Run("if isset position  success", func(t *testing.T) {
			for _, v := range casesSuccess {
				l := List{}
				l.items = append(l.items, v.in...)
				res := l.issetItemPosition(v.pos)
				require.Equal(t, v.out, res)
			}
		})
	})
	t.Run("TestIssetItemPositionUnEqual", func(t *testing.T) {
		casesNotEqual := []struct {
			in  []*Item
			pos int
			out bool
		}{
			{
				in:  []*Item{{1}, {2}},
				pos: 2,
				out: true,
			},
			{
				in:  []*Item{{1}, {2}, {"example"}},
				pos: 2,
				out: false,
			},
			{
				in:  []*Item{{1}, {2}, {"example"}},
				pos: 1,
				out: false,
			},
		}
		t.Run("if isset position  not equal", func(t *testing.T) {
			for _, v := range casesNotEqual {
				l := List{}
				l.items = append(l.items, v.in...)
				res := l.issetItemPosition(v.pos)
				require.NotEqual(t, v.out, res)
			}
		})
	})
}

func TestNext(t *testing.T) {
	t.Run("TestNextSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			in  []*Item
			pos int
			out *Item
		}{
			{
				in:  []*Item{{1}, {2}, {3}, {4}},
				pos: 1,
				out: &Item{3},
			},
			{
				in:  []*Item{{1}, {2}, {"example"}},
				pos: 0,
				out: &Item{2},
			},
			{
				in:  []*Item{{1}, {2}, {"example"}},
				pos: 1,
				out: &Item{"example"},
			},
			{
				in:  []*Item{{1}, {2}, {"example"}},
				pos: 2,
				out: nil,
			},
		}
		t.Run("if get next item  success", func(t *testing.T) {
			for _, v := range casesSuccess {
				l := List{}
				l.items = append(l.items, v.in...)
				l.pos = v.pos
				res := l.Next()
				require.Equal(t, v.out, res)
			}
		})
	})
	t.Run("TestNextUnEqual", func(t *testing.T) {
		casesNotEqual := []struct {
			in  []*Item
			pos int
			out *Item
		}{
			{
				in:  []*Item{{1}, {2}},
				pos: 2,
				out: &Item{2},
			},
			{
				in:  []*Item{{1}, {2}, {"example"}},
				pos: 2,
				out: &Item{"example"},
			},
			{
				in:  []*Item{{1}, {2}, {"example"}},
				pos: 1,
				out: &Item{2},
			},
		}
		t.Run("if get next item  not equal", func(t *testing.T) {
			for _, v := range casesNotEqual {
				l := List{}
				l.items = append(l.items, v.in...)
				l.pos = v.pos
				res := l.Next()
				require.NotEqual(t, v.out, res)
			}
		})
	})
}

func TestPrev(t *testing.T) {
	t.Run("TestPrevSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			in  []*Item
			pos int
			out *Item
		}{
			{
				in:  []*Item{{1}, {2}, {3}, {4}},
				pos: 1,
				out: &Item{1},
			},
			{
				in:  []*Item{{1}, {2}, {"example"}},
				pos: 0,
				out: nil,
			},
			{
				in:  []*Item{{1}, {2}, {"example"}},
				pos: 2,
				out: &Item{2},
			},
			{
				in:  []*Item{{1}, {2}, {"example"}},
				pos: 1,
				out: &Item{1},
			},
		}
		t.Run("if get prev item  success", func(t *testing.T) {
			for _, v := range casesSuccess {
				l := List{}
				l.items = append(l.items, v.in...)
				l.pos = v.pos
				res := l.Prev()
				require.Equal(t, v.out, res)
			}
		})
	})
	t.Run("TestPrevUnEqual", func(t *testing.T) {
		casesNotEqual := []struct {
			in  []*Item
			pos int
			out *Item
		}{
			{
				in:  []*Item{{1}, {2}},
				pos: 1,
				out: &Item{2},
			},
			{
				in:  []*Item{{1}, {2}, {"example"}},
				pos: 2,
				out: &Item{"example"},
			},
			{
				in:  []*Item{{1}, {2}, {"example"}},
				pos: 1,
				out: &Item{2},
			},
		}
		t.Run("if get prev item  not equal", func(t *testing.T) {
			for _, v := range casesNotEqual {
				l := List{}
				l.items = append(l.items, v.in...)
				l.pos = v.pos
				res := l.Prev()
				require.NotEqual(t, v.out, res)
			}
		})
	})
}
