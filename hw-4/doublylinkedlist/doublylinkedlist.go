package doublylinkedlist

type List struct {
	items []*Item
	pos   int
}

type Item struct {
	value interface{}
}

func (i *Item) GetItem() interface{} {
	return i.value
}

func (l *List) Len() int {
	return len(l.items)
}

func (l *List) First() *Item {
	return l.items[0]
}

func (l *List) Last() *Item {
	return l.items[len(l.items)-1]
}

func (l *List) PushFront(v interface{}) {
	l.pos = 0
	sl := append([]*Item(nil), &Item{v})
	l.items = append(sl, l.items...)
}

func (l *List) PushBack(v interface{}) {
	l.pos = len(l.items)
	l.items = append(l.items, &Item{v})
}

func (l *List) Remove(i int) *Item {
	if l.issetItemPosition(i) {
		rem := l.items[i]
		l.items = l.getSliceParts(i)
		return rem
	}
	return nil
}

func (l *List) getSliceParts(i int) []*Item {
	lPart := l.items[:i]
	rPart := l.items[i+1:]
	sl := append(lPart, rPart...)
	return sl
}

func (l *List) issetItemPosition(i int) bool {
	return i >= 0 && i < len(l.items)
}

func (l *List) Next() *Item {
	l.pos++
	if l.issetItemPosition(l.pos) {
		return l.items[l.pos]
	}
	l.pos = len(l.items) - 1
	return nil
}

func (l *List) Prev() *Item {
	l.pos--
	if l.issetItemPosition(l.pos) {
		return l.items[l.pos]
	}
	l.pos = 0
	return nil
}
