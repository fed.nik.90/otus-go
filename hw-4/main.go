package main

import (
	dll "gitlab.com/fed.nik.90/otus-go/hw-4/doublylinkedlist"
)

func main() {
	dllist := dll.List{}
	dllist.PushBack("test")
	dllist.PushBack(123)
	dllist.PushBack(321)
	dllist.PushBack(312)
	dllist.PushFront(111)
	dllist.Remove(3)
}
