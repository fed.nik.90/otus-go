module gitlab.com/fed.nik.90/otus-go/hw-11

go 1.14

require (
	github.com/gorilla/mux v1.8.0
	github.com/stretchr/testify v1.4.0
	go.uber.org/zap v1.15.0
	golang.org/x/tools v0.0.0-20191119224855-298f0cb1881e // indirect
	gopkg.in/yaml.v2 v2.2.2
)
