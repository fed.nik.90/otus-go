package helpers

import (
	"errors"
	"time"
)

func GetParseTimeFromString(start, end, format string) (time.Time, time.Time, error) {
	if !paramsIsNotEmpty(start, end) {
		return time.Time{}, time.Time{}, errors.New("empty string day time")
	}
	startDate, endDate, err := getParseDates(start, end, format)
	if err != nil {
		return time.Time{}, time.Time{}, err
	}
	return startDate, endDate, nil
}

func GetParseDateFromString(str, format string) (time.Time, error) {
	if !isNotEmpty(str) {
		return time.Time{}, errors.New("empty string day time")
	}
	date, err := getParseDate(str, format)
	if err != nil {
		return time.Time{}, err
	}
	return date, nil
}

func getParseDates(start, end, format string) (time.Time, time.Time, error) {
	startDate, err := getParseDate(start, format)
	if err != nil {
		return time.Time{}, time.Time{}, err
	}
	endDate, err := getParseDate(end, format)
	if err != nil {
		return time.Time{}, time.Time{}, err
	}
	return startDate, endDate, nil
}

func getParseDate(str, format string) (time.Time, error) {
	date, err := time.Parse(format, str)
	if err != nil {
		return time.Time{}, err
	}
	return date, nil
}

func paramsIsNotEmpty(s1, s2 string) bool {
	if isNotEmpty(s1) && isNotEmpty(s2) {
		return true
	}
	return false
}

func isNotEmpty(s string) bool {
	if s != "" {
		return true
	}
	return false
}
