package entities

import (
	"errors"
	"time"

	"gitlab.com/fed.nik.90/otus-go/hw-11/config"
)

type Event struct {
	ID          uint64
	Title       string
	Description string
	StartTime   time.Time
	EndTime     time.Time
}

type EventRepo struct {
	Event []Event
}

func (r *EventRepo) Increment() uint64 {
	l := uint64(len(r.Event))
	l++
	return l
}

func (r *EventRepo) Update(event Event) error {
	for k, v := range r.Event {
		if v.ID == event.ID {
			r.Event[k] = event
			return nil
		}
	}

	return errors.New("no such event for update")
}

func (r *EventRepo) Delete(event Event) error {
	for k, v := range r.Event {
		if v.ID == event.ID {
			copy(r.Event[k:], r.Event[k+1:])
			r.Event[len(r.Event)-1] = Event{}
			r.Event = r.Event[:len(r.Event)-1]
			return nil
		}
	}

	return errors.New("no such event for delete")
}

func (r *EventRepo) GetByID(id uint64) (Event, error) {
	event := Event{}
	for _, v := range r.Event {
		if v.ID == id {
			return v, nil
		}
	}

	return event, errors.New("id not found")
}

func (r *EventRepo) ShowByDay(date time.Time) ([]Event, error) {
	var sl []Event
	ok := false
	for _, v := range r.Event {
		start, err := time.Parse(config.FormatDate, v.StartTime.Format(config.FormatDate))
		if err != nil {
			return sl, err
		}
		end, err := time.Parse(config.FormatDate, v.EndTime.Format(config.FormatDate))
		if err != nil {
			return sl, err
		}
		if start.Unix() <= date.Unix() && date.Unix() <= end.Unix() {
			sl = append(sl, v)
			ok = true
		}
	}
	if ok {
		return sl, nil
	}

	return sl, errors.New("no such events for day")
}

func (r *EventRepo) ShowByPeriod(start, end time.Time) ([]Event, error) {
	var sl []Event
	ok := false
	for _, v := range r.Event {
		startDate, err := time.Parse(config.FormatDay, v.StartTime.Format(config.FormatDay))
		if err != nil {
			return sl, err
		}
		endDate, err := time.Parse(config.FormatDay, v.EndTime.Format(config.FormatDay))
		if err != nil {
			return sl, err
		}
		if start.Unix() <= startDate.Unix() && startDate.Unix() <= end.Unix() || start.Unix() <= endDate.Unix() && endDate.Unix() <= end.Unix() {
			sl = append(sl, v)
			ok = true
		}
	}
	if ok {
		return sl, nil
	}

	return sl, errors.New("no such events for period")
}

func (r *EventRepo) ShowByMonth(date time.Time) ([]Event, error) {
	var sl []Event
	ok := false
	for _, v := range r.Event {
		start, err := time.Parse(config.FormatMonth, v.StartTime.Format(config.FormatMonth))
		if err != nil {
			return sl, err
		}
		end, err := time.Parse(config.FormatMonth, v.EndTime.Format(config.FormatMonth))
		if err != nil {
			return sl, err
		}
		if start.Unix() <= date.Unix() && date.Unix() <= end.Unix() {
			sl = append(sl, v)
			ok = true
		}
	}
	if ok {
		return sl, nil
	}

	return sl, errors.New("no such events for month")
}

func (r *EventRepo) Create(event Event) error {
	r.Event = append(r.Event, event)
	if len(r.Event) == 0 {
		return errors.New("event not created")
	}

	return nil
}
