package logger

import (
	"strings"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type Settings struct {
	File  string
	Level string
}

var Logger *zap.Logger

func Configure(s Settings) (err error) {
	var level zap.AtomicLevel
	switch strings.ToLower(s.Level) {
	case "warn":
		level = zap.NewAtomicLevelAt(zap.WarnLevel)
	case "error":
		level = zap.NewAtomicLevelAt(zap.ErrorLevel)
	case "debug":
		level = zap.NewAtomicLevelAt(zap.DebugLevel)
	default:
		level = zap.NewAtomicLevelAt(zap.InfoLevel)
	}

	out := []string{"stdout"}
	if s.File != "" {
		out = append(out, s.File)
	}

	cfg := zap.Config{
		Level:       level,
		Encoding:    "json",
		OutputPaths: out,
		EncoderConfig: zapcore.EncoderConfig{
			MessageKey:   "message",
			LevelKey:     "level",
			EncodeLevel:  zapcore.CapitalLevelEncoder,
			TimeKey:      "time",
			EncodeTime:   zapcore.ISO8601TimeEncoder,
			CallerKey:    "caller",
			EncodeCaller: zapcore.ShortCallerEncoder,
		},
	}

	Logger, err = cfg.Build()
	if err != nil {
		return
	}

	_ = Logger.Sync()

	return
}
