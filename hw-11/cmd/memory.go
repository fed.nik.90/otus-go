package cmd

import (
	"gitlab.com/fed.nik.90/otus-go/hw-11/internal/entities"
	"gitlab.com/fed.nik.90/otus-go/hw-11/internal/usecases"
)

type Memory struct {
	action *usecases.EventUsecases
	memory *entities.EventRepo
}

func CreateMem() *Memory {
	er := new(entities.EventRepo)
	eu := new(usecases.EventUsecases)
	eu.EventRepository = er
	return &Memory{action: eu, memory: er}
}
