package cmd

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/fed.nik.90/otus-go/hw-11/internal/entities"
	"gitlab.com/fed.nik.90/otus-go/hw-11/pkg/logger"
)

const (
	statusOk    = "success"
	statusError = "error"
)

type Response struct {
	Event  entities.Event   `json:"event,omitempty"`
	Events []entities.Event `json:"events,omitempty"`
	Error  string           `json:"error,omitempty"`
	Status string           `json:"status,omitempty"`
}

func routers(router *mux.Router) {
	router.HandleFunc("/hello", helloHandler)
	router.HandleFunc("/create_event", middlewareCreateEvent(createEventHandler))
	router.HandleFunc("/update_event", middlewareUpdateEvent(updateEventHandler))
	router.HandleFunc("/delete_event", middlewareDeleteEvent(deleteEventHandler))
	router.HandleFunc("/events_for_day", middlewareEventsForDay(eventsForDayHandler)).Queries("date", "{date}")
	router.HandleFunc("/events_for_week", middlewareEventsForWeek(eventsForWeekHandler)).Queries("start", "{start}", "end", "{end}")
	router.HandleFunc("/events_for_month", middlewareEventsForMonth(eventsForMonthHandler)).Queries("date", "{date}")
}

func helloHandler(w http.ResponseWriter, r *http.Request) {
	_, err := w.Write([]byte("Hello World!"))
	if err != nil {
		logger.Logger.Fatal(err.Error())
	}
}

//curl -d 'title=title&description=description&start=2020-09-20 14:00&end=2020-10-01 15:00' -X POST http://127.0.0.1:8008/create_event
func createEventHandler(w http.ResponseWriter, r *http.Request) {
	values := r.Context().Value("values").(map[string]string)
	dates := r.Context().Value("dates").(map[string]time.Time)
	event, err := mem.action.AddEvent(values["title"], values["description"], dates["start"], dates["end"])
	resp := Response{}
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp.Status = statusError
		resp.Error = err.Error()
	}
	resp.Event = event
	resp.Status = statusOk
	w.WriteHeader(http.StatusOK)
	setResponse(resp, w)
}

//curl -d 'id=1&title=title_edit&description=description_edit&start=2020-09-19 12:00&end=2020-10-02 15:00' -X POST http://127.0.0.1:8008/update_event
func updateEventHandler(w http.ResponseWriter, r *http.Request) {
	id := r.Context().Value("id").(uint64)
	values := r.Context().Value("values").(map[string]string)
	dates := r.Context().Value("dates").(map[string]time.Time)
	event, err := mem.action.UpdateEvent(id, values["title"], values["description"], dates["start"], dates["end"])
	resp := Response{}
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp.Status = statusError
		resp.Error = err.Error()
	}
	resp.Event = event
	resp.Status = statusOk
	w.WriteHeader(http.StatusOK)
	setResponse(resp, w)
}

//curl -d 'id=1' -X POST http://127.0.0.1:8008/delete_event
func deleteEventHandler(w http.ResponseWriter, r *http.Request) {
	id := r.Context().Value("id").(uint64)
	err := mem.action.DeleteEvent(id)
	resp := Response{}
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp.Status = statusError
		resp.Error = err.Error()
	}
	resp.Status = statusOk
	w.WriteHeader(http.StatusOK)
	setResponse(resp, w)
}

//curl http://127.0.0.1:8008/events_for_day?date=2020-09-21
func eventsForDayHandler(w http.ResponseWriter, r *http.Request) {
	day := r.Context().Value("date").(time.Time)
	events, err := mem.action.ShowEventByDay(day)
	resp := Response{}
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp.Status = statusError
		resp.Error = err.Error()
	}
	resp.Events = events
	resp.Status = statusOk
	w.WriteHeader(http.StatusOK)
	setResponse(resp, w)
}

//curl http://127.0.0.1:8008/events_for_week?start=2020-09-20&end=2020-09-27
func eventsForWeekHandler(w http.ResponseWriter, r *http.Request) {
	dates := r.Context().Value("dates").(map[string]time.Time)
	events, err := mem.action.ShowEventByPeriod(dates["start"], dates["end"])
	resp := Response{}
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp.Status = statusError
		resp.Error = err.Error()
	}
	resp.Events = events
	resp.Status = statusOk
	w.WriteHeader(http.StatusOK)
	setResponse(resp, w)
}

//curl http://127.0.0.1:8008/events_for_month?date=2020-09
func eventsForMonthHandler(w http.ResponseWriter, r *http.Request) {
	date := r.Context().Value("date").(time.Time)
	events, err := mem.action.ShowEventByMonth(date)
	resp := Response{}
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp.Status = statusError
		resp.Error = err.Error()
	}
	resp.Events = events
	resp.Status = statusOk
	w.WriteHeader(http.StatusOK)
	setResponse(resp, w)
}

func setResponse(resp Response, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json")
	jsonResp, err := json.Marshal(resp)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		logger.Logger.Fatal(err.Error())
	}
	_, err = w.Write(jsonResp)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		logger.Logger.Fatal(err.Error())
	}
}
