package usecases

import (
	"fmt"
	"time"

	"gitlab.com/fed.nik.90/otus-go/hw-8/internal/entities"
	"gitlab.com/fed.nik.90/otus-go/hw-8/internal/interfaces"
)

type EventUsecases struct {
	EventRepository interfaces.EventRepository
}

func (eu *EventUsecases) AddEvent(title, description string, start, end time.Time) error {
	rec := entities.Event{
		ID:          eu.EventRepository.Increment(),
		Title:       title,
		Description: description,
		StartTime:   start,
		EndTime:     end,
	}

	err := eu.EventRepository.Create(rec)
	if err != nil {
		fmt.Println(err)
	}

	return nil
}

func (eu *EventUsecases) UpdateEvent(id uint64, title, description string, start, end time.Time) (entities.Event, error) {
	event, err := eu.EventRepository.GetByID(id)
	if err != nil {
		return entities.Event{}, err
	}
	event.Title = title
	event.Description = description
	event.StartTime = start
	event.EndTime = end
	err = eu.EventRepository.Update(event)
	if err != nil {
		fmt.Println(err)
	}

	return event, nil
}

func (eu *EventUsecases) DeleteEvent(id uint64) error {
	event, err := eu.EventRepository.GetByID(id)
	if err != nil {
		return err
	}
	err = eu.EventRepository.Delete(event)
	if err != nil {
		return err
	}
	return nil
}

func (eu *EventUsecases) ShowEventByDay(date time.Time) ([]entities.Event, error) {
	events, err := eu.EventRepository.ShowByDay(date)
	if err != nil {
		fmt.Println(err)
	}

	return events, nil
}

func (eu *EventUsecases) GetEventByID(id uint64) (entities.Event, error) {
	event, err := eu.EventRepository.GetByID(id)
	if err != nil {
		return entities.Event{}, err
	}

	return event, nil
}
