package cmd

import (
	"fmt"
	"time"

	"gitlab.com/fed.nik.90/otus-go/hw-8/internal/entities"
	"gitlab.com/fed.nik.90/otus-go/hw-8/internal/usecases"
)

func Start() {
	events := new(usecases.EventUsecases)
	events.EventRepository = new(entities.EventRepo)

	err := events.AddEvent("Event title 1", "Event description text", time.Date(2020, time.August, 15, 10, 00, 00, 00, time.Local), time.Date(2020, time.August, 16, 10, 00, 00, 00, time.Local))
	//err = events.AddEvent("Event title 2", "Event description text", time.Date(2020, time.August, 16, 10, 00, 00, 00, time.Local), time.Date(2020, time.August, 18, 10, 00, 00, 00, time.Local))
	//err = events.AddEvent("Event title 3", "Event description text", time.Date(2020, time.August, 18, 10, 00, 00, 00, time.Local), time.Date(2020, time.August, 21, 10, 00, 00, 00, time.Local))
	//err = events.AddEvent("Event title 4", "Event description text", time.Date(2020, time.August, 17, 10, 00, 00, 00, time.Local), time.Date(2020, time.August, 20, 10, 00, 00, 00, time.Local))
	//err = events.AddEvent("Event title 5", "Event description text", time.Date(2020, time.August, 11, 10, 00, 00, 00, time.Local), time.Date(2020, time.August, 12, 10, 00, 00, 00, time.Local))
	if err != nil {
		fmt.Println(err)
	}

	//_, err = events.GetEventByID(3)
	//if err != nil {
	//	fmt.Println(err)
	//}
	//fmt.Println(event)

	//_, err = events.UpdateEvent(2, "Event title 2_1", "Event description text", time.Now(), time.Now())
	//if err != nil {
	//	fmt.Println(err)
	//}
	//fmt.Println(event)

	//err = events.DeleteEvent(2)
	//if err != nil {
	//	fmt.Println(err)
	//}
	//fmt.Println(event)

	//ed, err := events.ShowEventByDay(time.Date(2020,time.August,20,10,00,00,00, time.Local))
	//if err != nil {
	//	fmt.Println(err)
	//}

	fmt.Println(events.EventRepository)
}
