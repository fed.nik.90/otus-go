package main

import (
	"log"
	"os"

	"gitlab.com/fed.nik.90/otus-go/hw-7/reader"
	"gitlab.com/fed.nik.90/otus-go/hw-7/runner"
)

const (
	minArgs = 3
)

func checkIsDir(path os.FileInfo) bool {
	return path.IsDir()
}

func checkIsExist(path string) (os.FileInfo, error) {
	elem, err := os.Stat(path)
	if os.IsNotExist(err) {
		return nil, err
	}
	return elem, nil
}

func main() {
	args := os.Args
	if len(args) < minArgs {
		log.Fatal("For example use: ./go-envdir envdir/env/ /bin/bash envdir/echo.sh arg1 arg2 ...")
	}
	path := args[1]
	cmd := args[2:]

	elem, err := checkIsExist(path)
	if err != nil {
		log.Fatal(err)
	}
	if !checkIsDir(elem) {
		log.Fatal("is not a directory")
	}
	env, err := reader.ReadDir(path)
	if err != nil {
		log.Fatal(err)
	}
	code := runner.RunCmd(cmd, env)
	os.Exit(code)
}
