package runner

import (
	"os"
	"os/exec"
)

var (
	CmdSuccess = 0
	CmdFailure = 1
)

func RunCmd(cmd []string, env map[string]string) int {
	if len(cmd) == 0 {
		return CmdFailure
	}

	name, args := cmd[0], cmd[1:]

	proc := exec.Command(name, args...)
	proc.Stderr = os.Stderr
	proc.Stdout = os.Stdout
	proc.Stdin = os.Stdin

	if env != nil {
		envs := make([]string, 0, len(env))
		for k, v := range env {
			envs = append(envs, k+"="+v)
		}

		proc.Env = envs
	}

	if err := proc.Run(); err != nil {
		return CmdFailure
	}

	return CmdSuccess
}
