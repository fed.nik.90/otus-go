package runner

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestRunCmd(t *testing.T) {
	t.Run("TestRunCmdSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			inCmd []string
			inEnv map[string]string
			out   int
		}{
			{
				inCmd: []string{"/bin/bash", "../envdir/echo.sh", "arg1=1", "arg2=2", "arg3=3", "arg4=4"},
				inEnv: map[string]string{"TEST": "test_param"},
				out:   0,
			},
			{
				inCmd: []string{"/bin/bash", "../envdir/echo.sh", "param1=1", "param2=2", "param3=3", "param4=4"},
				inEnv: map[string]string{"TEST": "another_param"},
				out:   0,
			},
		}
		t.Run("run commands with args from file  success", func(t *testing.T) {
			for _, v := range casesSuccess {
				res := RunCmd(v.inCmd, v.inEnv)
				require.Equal(t, v.out, res)
			}
		})
	})
	t.Run("TestRunCmdUnEqual", func(t *testing.T) {
		casesNotEqual := []struct {
			inCmd []string
			inEnv map[string]string
			out   int
		}{
			{
				inCmd: []string{"", "../envdir/echo.sh", "arg1=1", "arg2=2", "arg3=3", "arg4=4"},
				inEnv: map[string]string{"TEST": "test_param"},
				out:   0,
			},
			{
				inCmd: []string{"/bin/bash", "", "param1=1", "param2=2", "param3=3", "param4=4"},
				inEnv: map[string]string{"TEST": "another_param"},
				out:   0,
			},
			{
				inCmd: []string{},
				inEnv: map[string]string{"TEST": "another_param"},
				out:   0,
			},
		}
		t.Run("run commands with args from file  no equal", func(t *testing.T) {
			for _, v := range casesNotEqual {
				res := RunCmd(v.inCmd, v.inEnv)
				require.NotEqual(t, v.out, res)
			}
		})
	})
}
