package reader

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func searchInMap(m map[string]string, str string) bool {
	if _, ok := m[str]; ok {
		return true
	}
	return false
}

func TestReadDir(t *testing.T) {
	t.Run("TestReadDirSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			in  string
			out []string
		}{
			{
				in:  "../envdir",
				out: []string{"echo.sh"},
			},
			{
				in:  "../envdir/env",
				out: []string{"TEST"},
			},
		}
		t.Run("get list files from dir   no err success", func(t *testing.T) {
			for _, v := range casesSuccess {
				res := false
				dir, err := ReadDir(v.in)
				for _, vv := range v.out {
					if searchInMap(dir, vv) {
						res = true
					}
				}
				require.NoError(t, err)
				require.Equal(t, true, res)
			}
		})
	})
	t.Run("TestReadDirUnEqual", func(t *testing.T) {
		casesNotEqual := []struct {
			in  string
			out []string
		}{
			{
				in:  "../envdir",
				out: []string{"echo.sh"},
			},
			{
				in:  "../envdir/env",
				out: []string{"TEST"},
			},
		}
		t.Run("get list files from dir   no err no equal", func(t *testing.T) {
			for _, v := range casesNotEqual {
				res := false
				dir, err := ReadDir(v.in)
				for _, vv := range v.out {
					if searchInMap(dir, vv) {
						res = true
					}
				}
				require.NoError(t, err)
				require.NotEqual(t, false, res)
			}
		})
	})
	t.Run("TestReadDirError", func(t *testing.T) {
		casesError := []struct {
			in string
		}{
			{
				in: "./reader.go",
			},
			{
				in: "../go-envdir",
			},
			{
				in: "../awesome",
			},
		}
		for _, v := range casesError {
			t.Run("get list files from dir  err", func(t *testing.T) {
				_, err := ReadDir(v.in)
				require.Error(t, err)
			})
		}
	})
}
