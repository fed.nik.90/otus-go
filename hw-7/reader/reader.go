package reader

import (
	"io"
	"io/ioutil"
	"path/filepath"
)

type Env map[string]string

func ReadDir(dir string) (Env, error) {
	var result Env

	absPath, err := filepath.Abs(dir)
	if err != nil {
		return nil, err
	}

	files, err := ioutil.ReadDir(absPath)
	if err != nil {
		return nil, err
	}

	for _, file := range files {
		if file.IsDir() {
			continue
		}

		filename := file.Name()

		b, err := ioutil.ReadFile(filepath.Join(dir, filename))
		if err == io.EOF {
			delete(result, filename)
			continue
		}
		if err != nil {
			return nil, err
		}
		if len(b) == 0 {
			delete(result, filename)
			continue
		}
		if result == nil {
			result = make(Env)
		}
		result[filename] = string(b)
	}

	return result, err
}
