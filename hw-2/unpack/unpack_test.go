package unpack

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestGetNum(t *testing.T) {
	t.Run("TestGetNumSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			in  string
			out int
		}{
			{
				in:  "5",
				out: 5,
			},
			{
				in:  "10",
				out: 10,
			},
		}
		for _, v := range casesSuccess {
			t.Run("if string contains num  no err", func(t *testing.T) {
				res, err := getNum(v.in)
				require.NoError(t, err)
				require.Equal(t, v.out, res)
			})
		}
	})

	t.Run("TestGetNumUnEqual", func(t *testing.T) {
		casesNotEqual := []struct {
			in  string
			out int
		}{
			{
				in:  "5",
				out: 10,
			},
			{
				in:  "10",
				out: 5,
			},
		}
		for _, v := range casesNotEqual {
			t.Run("if string contains num  no equal no err", func(t *testing.T) {
				res, err := getNum(v.in)
				require.NoError(t, err)
				require.NotEqual(t, v.out, res)
			})
		}
	})

	t.Run("TestGetNumError", func(t *testing.T) {
		casesError := []struct {
			in string
		}{
			{
				in: "",
			},
			{
				in: " ",
			},
		}
		for _, v := range casesError {
			t.Run("if string contains num  err", func(t *testing.T) {
				_, err := getNum(v.in)
				require.Error(t, err)
			})
		}
	})
}

func TestIsNum(t *testing.T) {
	t.Run("TestIsNumSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			in  string
			out bool
		}{
			{
				in:  "5",
				out: true,
			},
			{
				in:  "10",
				out: true,
			},
			{
				in:  "1b",
				out: false,
			},
			{
				in:  " 1",
				out: false,
			},
			{
				in:  "abc",
				out: false,
			},
		}
		for _, v := range casesSuccess {
			t.Run("if string equal num  no err", func(t *testing.T) {
				res := isNum(v.in)
				require.Equal(t, v.out, res)
			})
		}
	})

	t.Run("TestIsNumUnEqual", func(t *testing.T) {
		casesNotEqual := []struct {
			in  string
			out bool
		}{
			{
				in:  "5",
				out: false,
			},
			{
				in:  "10",
				out: false,
			},
			{
				in:  "1b",
				out: true,
			},
			{
				in:  " 1",
				out: true,
			},
			{
				in:  "abc",
				out: true,
			},
		}
		for _, v := range casesNotEqual {
			t.Run("if string equal num  no equal no err", func(t *testing.T) {
				res := isNum(v.in)
				require.NotEqual(t, v.out, res)
			})
		}
	})
}

func TestCheckStr(t *testing.T) {
	t.Run("TestCheckStrSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			in  string
			out bool
		}{
			{
				in:  "a",
				out: true,
			},
			{
				in:  "abc",
				out: true,
			},
			{
				in:  "",
				out: false,
			},
			{
				in:  " ",
				out: false,
			},
			{
				in:  "123",
				out: false,
			},
			{
				in:  " 123",
				out: false,
			},
		}
		for _, v := range casesSuccess {
			t.Run("if string and checked  no err", func(t *testing.T) {
				res := checkStr(v.in)
				require.Equal(t, v.out, res)
			})
		}
	})

	t.Run("TestCheckStrUnEqual", func(t *testing.T) {
		casesNotEqual := []struct {
			in  string
			out bool
		}{
			{
				in:  "123",
				out: true,
			},
			{
				in:  "1a",
				out: true,
			},
			{
				in:  " ",
				out: true,
			},
			{
				in:  "abc",
				out: false,
			},
		}
		for _, v := range casesNotEqual {
			t.Run("if string and checked  no equal no err", func(t *testing.T) {
				res := checkStr(v.in)
				require.NotEqual(t, v.out, res)
			})
		}
	})
}

func TestExcludedSymbol(t *testing.T) {
	t.Run("TestExcludedSymbolSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			in  string
			out bool
		}{
			{
				in:  `\`,
				out: true,
			},
			{
				in:  `/`,
				out: false,
			},
		}
		for _, v := range casesSuccess {
			t.Run("if correct excluded symbol  no err", func(t *testing.T) {
				res := excludedSymbol(v.in)
				require.Equal(t, v.out, res)
			})
		}
	})

	t.Run("TestExcludedSymbolUnEqual", func(t *testing.T) {
		casesNotEqual := []struct {
			in  string
			out bool
		}{
			{
				in:  `\`,
				out: false,
			},
			{
				in:  `/`,
				out: true,
			},
		}
		for _, v := range casesNotEqual {
			t.Run("if correct excluded symbol  no equal no err", func(t *testing.T) {
				res := excludedSymbol(v.in)
				require.NotEqual(t, v.out, res)
			})
		}
	})
}

func TestGetNextElement(t *testing.T) {
	t.Run("TestGetNextElementSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			in  []string
			in2 int
			out string
		}{
			{
				in:  []string{"a", "2", "b", "3", "c"},
				in2: 0,
				out: "2",
			},
			{
				in:  []string{"a", "2", "b", "3", "c"},
				in2: 1,
				out: "b",
			},
			{
				in:  []string{"a", "2", "b", "3", "c"},
				in2: 4,
				out: "",
			},
		}
		for _, v := range casesSuccess {
			t.Run("if get next elem from slice  no err", func(t *testing.T) {
				res, _ := getNextElement(v.in, v.in2)
				require.Equal(t, v.out, res)
			})
		}
	})

	t.Run("TestGetNextElementUnEqual", func(t *testing.T) {
		casesNotEqual := []struct {
			in  []string
			in2 int
			out string
		}{
			{
				in:  []string{"a", "2", "b", "3", "c"},
				in2: 0,
				out: "a",
			},
			{
				in:  []string{"a", "2", "b", "3", "c"},
				in2: 1,
				out: "2",
			},
			{
				in:  []string{"a", "2", "b", "3", "c"},
				in2: 4,
				out: "c",
			},
		}
		for _, v := range casesNotEqual {
			t.Run("if get next elem from slice  no equal no err", func(t *testing.T) {
				res, _ := getNextElement(v.in, v.in2)
				require.NotEqual(t, v.out, res)
			})
		}
	})

	t.Run("TestGetNextElementError", func(t *testing.T) {
		casesError := []struct {
			in  []string
			in2 int
		}{
			{
				in:  []string{"a", "2", "b", "3", "c"},
				in2: 5,
			},
			{
				in:  []string{"a", "2", "b", "3", "c"},
				in2: 10,
			},
		}
		for _, v := range casesError {
			t.Run("if get next elem from slice  err", func(t *testing.T) {
				_, err := getNextElement(v.in, v.in2)
				require.Error(t, err)
			})
		}
	})
}

func TestGetPrevElement(t *testing.T) {
	t.Run("TestGetPrevElementSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			in  []string
			in2 int
			out string
		}{
			{
				in:  []string{"a", "2", "b", "3", "c"},
				in2: 1,
				out: "a",
			},
			{
				in:  []string{"a", "2", "b", "3", "c"},
				in2: 3,
				out: "b",
			},
			{
				in:  []string{"a", "2", "b", "3", "c"},
				in2: 0,
				out: "",
			},
		}
		for _, v := range casesSuccess {
			t.Run("if get prev elem from slice  no err", func(t *testing.T) {
				res, _ := getPrevElement(v.in, v.in2)
				require.Equal(t, v.out, res)
			})
		}
	})

	t.Run("TestGetPrevElementUnEqual", func(t *testing.T) {
		casesNotEqual := []struct {
			in  []string
			in2 int
			out string
		}{
			{
				in:  []string{"a", "2", "b", "3", "c"},
				in2: 0,
				out: "a",
			},
			{
				in:  []string{"a", "2", "b", "3", "c"},
				in2: 10,
				out: "c",
			},
			{
				in:  []string{"a", "2", "b", "3", "c"},
				in2: 4,
				out: "c",
			},
		}
		for _, v := range casesNotEqual {
			t.Run("if get prev elem from slice  no equal no err", func(t *testing.T) {
				res, _ := getPrevElement(v.in, v.in2)
				require.NotEqual(t, v.out, res)
			})
		}
	})

	t.Run("TestGetPrevElementError", func(t *testing.T) {
		casesError := []struct {
			in  []string
			in2 int
		}{
			{
				in:  []string{"a", "2", "b", "3", "c"},
				in2: 5,
			},
			{
				in:  []string{"a", "2", "b", "3", "c"},
				in2: 0,
			},
		}
		for _, v := range casesError {
			t.Run("if get prev elem from slice  err", func(t *testing.T) {
				_, err := getPrevElement(v.in, v.in2)
				require.Error(t, err)
			})
		}
	})
}

func TestIsNextElementIsNum(t *testing.T) {
	t.Run("TestIsNextElementIsNumSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			in  []string
			in2 int
			out bool
		}{
			{
				in:  []string{"a", "2", "b", "3", "c"},
				in2: 0,
				out: true,
			},
			{
				in:  []string{"a", "2", "b", "3", "c"},
				in2: 2,
				out: true,
			},
			{
				in:  []string{"a", "2", "b", "3", "c"},
				in2: 1,
				out: false,
			},
		}
		for _, v := range casesSuccess {
			t.Run("if next element is num  no err", func(t *testing.T) {
				res, _ := isNextElementIsNum(v.in, v.in2)
				require.Equal(t, v.out, res)
			})
		}
	})

	t.Run("TestIsNextElementIsNumUnEqual", func(t *testing.T) {
		casesNotEqual := []struct {
			in  []string
			in2 int
			out bool
		}{
			{
				in:  []string{"a", "2", "b", "3", "c"},
				in2: 1,
				out: true,
			},
			{
				in:  []string{"a", "2", "b", "3", "c"},
				in2: 10,
				out: true,
			},
			{
				in:  []string{"a", "2", "b", "3", "c"},
				in2: 4,
				out: true,
			},
		}
		for _, v := range casesNotEqual {
			t.Run("if next element is num  no equal no err", func(t *testing.T) {
				res, _ := isNextElementIsNum(v.in, v.in2)
				require.NotEqual(t, v.out, res)
			})
		}
	})

	t.Run("TestIsNextElementIsNumError", func(t *testing.T) {
		casesError := []struct {
			in  []string
			in2 int
		}{
			{
				in:  []string{},
				in2: 0,
			},
			{
				in:  []string{"a", "2", "b", "3", "c"},
				in2: 10,
			},
		}
		for _, v := range casesError {
			t.Run("if next element is num  err", func(t *testing.T) {
				_, err := isNextElementIsNum(v.in, v.in2)
				require.Error(t, err)
			})
		}
	})
}

func TestGetRepeatedNumbers(t *testing.T) {
	t.Run("TestGetRepeatedNumbersSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			in  []string
			in2 int
			out []string
		}{
			{
				in:  []string{`\`, `\`, "1"},
				in2: 0,
				out: []string{`\`},
			},
			{
				in:  []string{`\`, `\`, "4"},
				in2: 0,
				out: []string{`\\\\`},
			},
			{
				in:  []string{`\`, `\`, "3"},
				in2: 0,
				out: []string{`\\\`},
			},
		}
		for _, v := range casesSuccess {
			t.Run("if repeate excludedSymbol num  no err", func(t *testing.T) {
				res, _ := getRepeatedNumbers(v.in, v.in2)
				require.Equal(t, v.out, res)
			})
		}
	})

	t.Run("TestGetRepeatedNumbersUnEqual", func(t *testing.T) {
		casesNotEqual := []struct {
			in  []string
			in2 int
			out []string
		}{
			{
				in:  []string{`\`, `\`, "3"},
				in2: 0,
				out: []string{`\`},
			},
			{
				in:  []string{`\`, `\`, "2"},
				in2: 0,
				out: []string{`\`},
			},
			{
				in:  []string{`\`, `\`, "a"},
				in2: 0,
				out: []string{`\`},
			},
		}
		for _, v := range casesNotEqual {
			t.Run("if repeate excludedSymbol num  no equal no err", func(t *testing.T) {
				res, _ := getRepeatedNumbers(v.in, v.in2)
				require.NotEqual(t, v.out, res)
			})
		}
	})

	t.Run("TestGetRepeatedNumbersError", func(t *testing.T) {
		casesError := []struct {
			in  []string
			in2 int
		}{
			{
				in:  []string{},
				in2: 0,
			},
			{
				in:  []string{`\`, "2", "b", "3", "c"},
				in2: 10,
			},
		}
		for _, v := range casesError {
			t.Run("if repeate excludedSymbol num  err", func(t *testing.T) {
				_, err := getRepeatedNumbers(v.in, v.in2)
				require.Error(t, err)
			})
		}
	})
}

func TestGetShieldedNumbers(t *testing.T) {
	t.Run("TestGetShieldedNumbersSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			in  []string
			in2 int
			out []string
		}{
			{
				in:  []string{`\`, "1", `\`},
				in2: 0,
				out: []string{"1"},
			},
			{
				in:  []string{`\`, "2", `\`},
				in2: 0,
				out: []string{"2"},
			},
			{
				in:  []string{`\`, "3", `\`},
				in2: 0,
				out: []string{"3"},
			},
		}
		for _, v := range casesSuccess {
			t.Run("if num between excludedSymbols append num  no err", func(t *testing.T) {
				res, _ := getShieldedNumbers(v.in, v.in2)
				require.Equal(t, v.out, res)
			})
		}
	})

	t.Run("TestGetShieldedNumbersUnEqual", func(t *testing.T) {
		casesNotEqual := []struct {
			in  []string
			in2 int
			out []string
		}{
			{
				in:  []string{`\`, "1", `\`},
				in2: 0,
				out: []string{"3"},
			},
			{
				in:  []string{`\`, "2", `\`},
				in2: 0,
				out: []string{"1"},
			},
			{
				in:  []string{`\`, "3", `\`},
				in2: 0,
				out: []string{"2"},
			},
		}
		for _, v := range casesNotEqual {
			t.Run("if num between excludedSymbols append num  no equal no err", func(t *testing.T) {
				res, _ := getShieldedNumbers(v.in, v.in2)
				require.NotEqual(t, v.out, res)
			})
		}
	})

	t.Run("TestGetShieldedNumbersError", func(t *testing.T) {
		casesError := []struct {
			in  []string
			in2 int
		}{
			{
				in:  []string{},
				in2: 0,
			},
			{
				in:  []string{`\`, "2", `\`},
				in2: 10,
			},
		}
		for _, v := range casesError {
			t.Run("if num between excludedSymbols append num  err", func(t *testing.T) {
				_, err := getShieldedNumbers(v.in, v.in2)
				require.Error(t, err)
			})
		}
	})
}

func TestGetRepeatedSymbols(t *testing.T) {
	t.Run("TestGetRepeatedSymbolsSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			in  []string
			in2 int
			out []string
		}{
			{
				in:  []string{"1", "4", "a"},
				in2: 1,
				out: []string{"1111"},
			},
			{
				in:  []string{"2", "3", "b"},
				in2: 1,
				out: []string{"222"},
			},
			{
				in:  []string{"3", "5", "c"},
				in2: 1,
				out: []string{"33333"},
			},
			{
				in:  []string{"a", "5", "c"},
				in2: 1,
				out: []string{"aaaaa"},
			},
			{
				in:  []string{"a", "4", "b", "3", "c", "5"},
				in2: 3,
				out: []string{"bbb"},
			},
		}
		for _, v := range casesSuccess {
			t.Run("if repeat prev symbol and this or prev not excluded symbol  no err", func(t *testing.T) {
				res, _ := getRepeatedSymbols(v.in, v.in2)
				require.Equal(t, v.out, res)
			})
		}
	})

	t.Run("TestGetRepeatedSymbolsUnEqual", func(t *testing.T) {
		casesNotEqual := []struct {
			in  []string
			in2 int
			out []string
		}{
			{
				in:  []string{"a", "4", "b", "3", "c", "5"},
				in2: 5,
				out: []string{"bbb"},
			},
			{
				in:  []string{"a", "4", "b", "3", "c", "5"},
				in2: 1,
				out: []string{"aaa"},
			},
			{
				in:  []string{"a", "4", "b", "3", "c", "5"},
				in2: 3,
				out: []string{"bb"},
			},
		}
		for _, v := range casesNotEqual {
			t.Run("if repeat prev symbol and this or prev not excluded symbol  no equal no err", func(t *testing.T) {
				res, _ := getRepeatedSymbols(v.in, v.in2)
				require.NotEqual(t, v.out, res)
			})
		}
	})
	//TODO нужно ли делать проверку на ошибки...
}

func TestUnpackSymbols(t *testing.T) {
	t.Run("TestUnpackSymbolsSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			in  string
			out string
		}{
			{
				in:  "",
				out: "",
			},
			{
				in:  "abcd",
				out: "abcd",
			},
			{
				in:  "a4bc2d5e",
				out: "aaaabccddddde",
			},
			{
				in:  `qwe\4\5`,
				out: "qwe45",
			},
			{
				in:  `qwe\45`,
				out: "qwe44444",
			},
			{
				in:  `qwe\\5`,
				out: `qwe\\\\\`,
			},
		}
		for _, v := range casesSuccess {
			t.Run("if unpack symbols  no err", func(t *testing.T) {
				res, _ := unpackSymbols(v.in)
				require.Equal(t, v.out, res)
			})
		}
	})

	t.Run("TestUnpackSymbolsUnEqual", func(t *testing.T) {
		casesNotEqual := []struct {
			in  string
			out string
		}{
			{
				in:  "a4bc2d5e",
				out: "abcde",
			},
			{
				in:  "abc",
				out: "qwe",
			},
			{
				in:  "1qwe2",
				out: "1qwee",
			},
			{
				in:  " 123 ",
				out: "123",
			},
			{
				in:  " ",
				out: "",
			},
		}
		for _, v := range casesNotEqual {
			t.Run("if unpack symbols  no equal no err", func(t *testing.T) {
				res, _ := unpackSymbols(v.in)
				require.NotEqual(t, v.out, res)
			})
		}
	})

	t.Run("TestUnpackSymbolsError", func(t *testing.T) {
		casesError := []struct {
			in string
		}{
			{
				in: `\\\`,
			},
		}
		for _, v := range casesError {
			t.Run("if unpack symbols  err", func(t *testing.T) {
				_, err := unpackSymbols(v.in)
				require.Error(t, err)
			})
		}
	})
}

func TestGetUnpackStr(t *testing.T) {
	t.Run("TestGetUnpackStrSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			in  string
			out string
		}{
			{
				in:  "",
				out: "",
			},
			{
				in:  "abcd",
				out: "abcd",
			},
			{
				in:  "a4bc2d5e",
				out: "aaaabccddddde",
			},
			{
				in:  `qwe\4\5`,
				out: "qwe45",
			},
			{
				in:  `qwe\45`,
				out: "qwe44444",
			},
			{
				in:  `qwe\\5`,
				out: `qwe\\\\\`,
			},
		}
		for _, v := range casesSuccess {
			t.Run("if unpack str and return result  no err", func(t *testing.T) {
				res, _ := GetUnpackStr(v.in)
				require.Equal(t, v.out, res)
			})
		}
	})

	t.Run("TestGetUnpackStrUnEqual", func(t *testing.T) {
		casesNotEqual := []struct {
			in  string
			out string
		}{
			{
				in:  "a4bc2d5e",
				out: "abcde",
			},
			{
				in:  "abc",
				out: "qwe",
			},
			{
				in:  "1qwe2",
				out: "1qwee",
			},
			{
				in:  " 123 ",
				out: "123",
			},
			{
				in:  " ",
				out: " ",
			},
		}
		for _, v := range casesNotEqual {
			t.Run("if unpack str and return result  no equal no err", func(t *testing.T) {
				res, _ := GetUnpackStr(v.in)
				require.NotEqual(t, v.out, res)
			})
		}
	})

	t.Run("TestGetUnpackStrError", func(t *testing.T) {
		casesError := []struct {
			in string
		}{
			{
				in: `\\\`,
			},
			{
				in: " ",
			},
			{
				in: "",
			},
			{
				in: "123",
			},
		}
		for _, v := range casesError {
			t.Run("if unpack str and return result  err", func(t *testing.T) {
				_, err := GetUnpackStr(v.in)
				require.Error(t, err)
			})
		}
	})
}
