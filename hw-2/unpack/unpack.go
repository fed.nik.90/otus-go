package unpack

import (
	"errors"
	"strconv"
	"strings"
)

const (
	excludeSymbol = `\`
)

func GetUnpackStr(str string) (unpackStr string, err error) {
	if !checkStr(str) {
		return unpackStr, errors.New("invalid or empty string")
	}
	unpackStr, err = unpackSymbols(str)
	if err != nil {
		return "", err
	}
	return
}

func unpackSymbols(str string) (string, error) {
	var s = strings.Split(str, "")
	res := make([]string, 0, len(s)*3)

	for i := range s {
		tmp, err := getUnhandledSymbols(s, i)
		if err != nil {
			return "", err
		}
		res = append(res, tmp...)
		tmp, err = getSliceIfCurrentExcluded(s, i)
		if err != nil {
			return "", err
		}
		res = append(res, tmp...)
		tmp, err = getRepeatedSymbols(s, i)
		if err != nil {
			return "", err
		}
		res = append(res, tmp...)
	}
	return strings.Join(res, ""), nil
}

func getUnhandledSymbols(sliceStr []string, i int) ([]string, error) {
	sliceLength := len(sliceStr) - 1
	if !excludedSymbol(sliceStr[i]) && !isNum(sliceStr[i]) {
		var elem string
		if i == sliceLength {
			elem = sliceStr[i]
		} else {
			nextElem, errNextElem := getNextElement(sliceStr, i)
			if errNextElem != nil {
				return nil, errNextElem
			}
			elem = nextElem
		}
		if (i+1) <= sliceLength && !isNum(elem) || i == sliceLength {
			return append([]string(nil), sliceStr[i]), nil
		}
	}
	return nil, nil
}

func getSliceIfCurrentExcluded(s []string, i int) ([]string, error) {
	ln := len(s)
	res := make([]string, 0, ln)

	if excludedSymbol(s[i]) {
		isNextNum, errIsNextNum := isNextElementIsNum(s, i)
		if errIsNextNum != nil {
			return nil, errIsNextNum
		}
		getPrevElem, errGetPrevElem := getPrevElement(s, i)
		if errGetPrevElem != nil {
			return nil, errGetPrevElem
		}
		if (i+1) <= ln-1 && (i+2) <= ln-1 {
			shieldNumbers, errShieldNumbers := getShieldedNumbers(s, i)
			if errShieldNumbers != nil {
				return nil, errShieldNumbers
			}
			res = append(res, shieldNumbers...)
			repeatedNumbers, err := getRepeatedNumbers(s, i)
			if err != nil {
				return nil, err
			}
			res = append(res, repeatedNumbers...)
		}
		if (i+1) == ln-1 && isNextNum && getPrevElem != excludeSymbol {
			nextElem, errNextElem := getNextElement(s, i)
			if errNextElem != nil {
				return nil, errNextElem
			}
			res = append(res, nextElem)
		}
	}

	return res, nil
}

func getRepeatedSymbols(s []string, i int) ([]string, error) {
	var elem string
	if i == 0 {
		elem = ""
	} else {
		getPrevElem, errGetPrevElem := getPrevElement(s, i)
		if errGetPrevElem != nil {
			return nil, errGetPrevElem
		}
		elem = getPrevElem
	}
	if isNum(s[i]) && elem != excludeSymbol {
		num, err := getNum(s[i])
		if err != nil {
			return nil, err
		}
		return append([]string(nil), strings.Repeat(elem, num)), nil
	}
	return nil, nil
}

func getShieldedNumbers(sliceStr []string, i int) ([]string, error) {
	isNextNum, errIsNextNum := isNextElementIsNum(sliceStr, i)
	if errIsNextNum != nil {
		return nil, errIsNextNum
	}
	if isNextNum && sliceStr[i+2] == excludeSymbol {
		nextElem, errNextElem := getNextElement(sliceStr, i)
		if errNextElem != nil {
			return nil, errNextElem
		}
		return append([]string(nil), nextElem), nil
	}
	return nil, nil
}

func getRepeatedNumbers(sliceStr []string, i int) ([]string, error) {
	nextElem, errNextElem := getNextElement(sliceStr, i)
	if errNextElem != nil {
		return nil, errNextElem
	}
	if isNum(sliceStr[i+2]) && nextElem == excludeSymbol {
		num, err := getNum(sliceStr[i+2])
		if err != nil {
			return nil, err
		}
		return append([]string(nil), strings.Repeat(nextElem, num)), nil
	}
	return nil, nil
}

func getPrevElement(sliceStr []string, i int) (string, error) {
	if i > 0 && i <= len(sliceStr)-1 {
		return sliceStr[i-1], nil
	}
	return "", errors.New("getPrevElement index out of range")
}

func getNextElement(sliceStr []string, i int) (string, error) {
	if i >= 0 && i+1 <= len(sliceStr)-1 {
		return sliceStr[i+1], nil
	}
	return "", errors.New("getNextElement index out of range")
}

func isNextElementIsNum(sliceStr []string, i int) (bool, error) {
	nextElem, errNextElem := getNextElement(sliceStr, i)
	if errNextElem != nil {
		return false, errNextElem
	}
	return isNum(nextElem), nil
}

func isNum(str string) bool {
	_, errStr := strconv.Atoi(str)
	return errStr == nil
}

func getNum(str string) (int, error) {
	num, err := strconv.Atoi(str)
	if err != nil {
		return 0, err
	}
	return num, nil
}

func checkStr(str string) bool {
	str = strings.TrimSpace(str)
	if len(str) == 0 {
		return false
	}
	_, errFirstSymbol := strconv.Atoi(string(str[0]))
	_, errStr := strconv.Atoi(str)
	if errStr == nil || errFirstSymbol == nil {
		return false
	}
	return true
}

func excludedSymbol(symbol string) bool {
	return symbol == excludeSymbol
}
