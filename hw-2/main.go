package main

import (
	"fmt"
	"os"

	"gitlab.com/fed.nik.90/otus-go/hw-2/unpack"
)

func main() {
	var str = `a4bc2d5e`
	//var str = `abcd`
	//var str = ``
	//var str = `45`
	//var str = `qwe\4\5`//qwe45
	//var str = `qwe\45`//qwe44444
	//var str = `qwe\\5`//qwe\\\\\

	unpackStr, err := unpack.GetUnpackStr(str)
	if err != nil {
		fmt.Printf("Errors: %v", err)
		os.Exit(1)
	}
	fmt.Printf("Unpack string: %v\n", unpackStr)
}
