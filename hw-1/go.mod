module gitlab.com/fed.nik.90/otus-go/hw-1

go 1.13

require (
	github.com/beevik/ntp v0.3.0
	github.com/stretchr/testify v1.5.1 // indirect
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e // indirect
)
