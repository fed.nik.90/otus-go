package main

import (
	"flag"
	"log"

	"gitlab.com/fed.nik.90/otus-go/hw-9/cmd"
	"gitlab.com/fed.nik.90/otus-go/hw-9/config"
	"gitlab.com/fed.nik.90/otus-go/hw-9/pkg/logger"
)

var pathConfig string

func init() {
	flag.StringVar(&pathConfig, "pathConfig", "./config/config.yaml", "path config")
}

func main() {
	flag.Parse()

	conf, err := config.ParseConfig(pathConfig)
	if err != nil {
		log.Fatalf("can't parse config: %v", err)
	}

	err = logger.Configure(logger.Settings{File: conf.LogFile, Level: conf.LogLevel})
	if err != nil {
		log.Fatalf("can't config logger: %v", err)
	}

	cmd.Start()
	cmd.Web(conf)
}
