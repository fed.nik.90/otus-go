package cmd

import (
	"gitlab.com/fed.nik.90/otus-go/hw-9/internal/entities"
	"gitlab.com/fed.nik.90/otus-go/hw-9/internal/usecases"
)

func Start() {
	events := new(usecases.EventUsecases)
	events.EventRepository = new(entities.EventRepo)
}
