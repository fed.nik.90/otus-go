package cmd

import (
	"github.com/gorilla/mux"
	"gitlab.com/fed.nik.90/otus-go/hw-9/config"
	"gitlab.com/fed.nik.90/otus-go/hw-9/pkg/logger"
	"go.uber.org/zap"

	"net/http"
)

func Web(conf *config.Config) {
	listenAddr := conf.ListenIP + ":" + conf.ListenPort
	router := mux.NewRouter()
	router.HandleFunc("/hello", func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte("Hello World!"))
		if err != nil {
			logger.Logger.Fatal(err.Error())
		}
	})

	logger.Logger.Info("Web server starting", zap.String("ip", listenAddr))

	webServer := &http.Server{Addr: listenAddr, Handler: router}
	err := webServer.ListenAndServe()
	if err != nil {
		logger.Logger.Panic(err.Error())
	}
}
