package entities

import (
	"errors"
	"time"
)

type Event struct {
	ID          uint64
	Title       string
	Description string
	StartTime   time.Time
	EndTime     time.Time
}

type EventRepo struct {
	sl []Event
}

func (r *EventRepo) Increment() uint64 {
	l := uint64(len(r.sl))
	l++
	return l
}

func (r *EventRepo) Update(event Event) error {
	for k, v := range r.sl {
		if v.ID == event.ID {
			r.sl[k] = event
			return nil
		}
	}

	return errors.New("no such event for update")
}

func (r *EventRepo) Delete(event Event) error {
	for k, v := range r.sl {
		if v.ID == event.ID {
			copy(r.sl[k:], r.sl[k+1:])
			r.sl[len(r.sl)-1] = Event{}
			r.sl = r.sl[:len(r.sl)-1]
			return nil
		}
	}

	return errors.New("no such event for delete")
}

func (r *EventRepo) GetByID(id uint64) (Event, error) {
	event := Event{}
	for _, v := range r.sl {
		if v.ID == id {
			return v, nil
		}
	}

	return event, errors.New("id not found")
}

func (r *EventRepo) ShowByDay(date time.Time) ([]Event, error) {
	var sl []Event
	ok := false
	for _, v := range r.sl {
		if v.StartTime.Unix() <= date.Unix() && date.Unix() <= v.EndTime.Unix() {
			sl = append(sl, v)
			ok = true
		}
	}
	if ok {
		return sl, nil
	}

	return sl, errors.New("no such events for day")
}

func (r *EventRepo) Create(event Event) error {
	r.sl = append(r.sl, event)
	if len(r.sl) == 0 {
		return errors.New("event not created")
	}

	return nil
}
