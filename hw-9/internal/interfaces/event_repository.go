package interfaces

import (
	"time"

	"gitlab.com/fed.nik.90/otus-go/hw-9/internal/entities"
)

type EventRepository interface {
	Increment() uint64
	Create(event entities.Event) error
	Update(event entities.Event) error
	Delete(event entities.Event) error
	GetByID(id uint64) (entities.Event, error)
	ShowByDay(date time.Time) ([]entities.Event, error)
}
