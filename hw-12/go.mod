module gitlab.com/fed.nik.90/otus-go/hw-12

go 1.14

require (
	github.com/golang/protobuf v1.4.3
	github.com/gorilla/mux v1.8.0
	go.uber.org/zap v1.15.0
	golang.org/x/tools v0.0.0-20191119224855-298f0cb1881e // indirect
	google.golang.org/grpc v1.33.0
	google.golang.org/protobuf v1.25.0
	gopkg.in/yaml.v2 v2.2.2
)
