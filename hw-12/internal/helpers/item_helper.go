package helpers

import (
	"errors"
	"strconv"
	"strings"
)

func CheckString(s1, s2 string) (string, string, error) {
	str1 := trimString(s1)
	str2 := trimString(s2)
	if str1 == "" && str2 == "" {
		return str1, str2, errors.New("empty string")
	}
	return str1, str2, nil
}

func trimString(str string) string {
	return strings.TrimSpace(str)
}

func GetParseIDFromString(id string) (uint64, error) {
	n, err := strconv.ParseUint(id, 10, 64)
	if err != nil {
		return 0, err
	}
	return n, nil
}
