package usecases

import (
	"time"

	"gitlab.com/fed.nik.90/otus-go/hw-12/internal/entities"
	"gitlab.com/fed.nik.90/otus-go/hw-12/internal/interfaces"
	"gitlab.com/fed.nik.90/otus-go/hw-12/pkg/logger"
	"go.uber.org/zap"
)

type EventUsecases struct {
	EventRepository interfaces.EventRepository
}

func (eu *EventUsecases) AddEvent(title, description string, start, end time.Time) (entities.Event, error) {
	rec := entities.Event{
		ID:          eu.EventRepository.Increment(),
		Title:       title,
		Description: description,
		StartTime:   start,
		EndTime:     end,
	}

	err := eu.EventRepository.Create(rec)
	if err != nil {
		logger.Logger.Info("error create event")
		return entities.Event{}, err
	}

	return rec, nil
}

func (eu *EventUsecases) UpdateEvent(id uint64, title, description string, start, end time.Time) (entities.Event, error) {
	event, err := eu.EventRepository.GetByID(id)
	if err != nil {
		logger.Logger.Info("error on get event by id in update event")
		return entities.Event{}, err
	}
	event.Title = title
	event.Description = description
	event.StartTime = start
	event.EndTime = end

	err = eu.EventRepository.Update(event)
	if err != nil {
		logger.Logger.Info("error update event")
		return entities.Event{}, err
	}

	return event, nil
}

func (eu *EventUsecases) DeleteEvent(id uint64) error {
	event, err := eu.EventRepository.GetByID(id)
	if err != nil {
		logger.Logger.Info("error on get event by id in delete event")
		return err
	}
	err = eu.EventRepository.Delete(event)
	if err != nil {
		logger.Logger.Info("error delete event")
		return err
	}
	return nil
}

func (eu *EventUsecases) ShowEventByDay(date time.Time) ([]entities.Event, error) {
	events, err := eu.EventRepository.ShowByDay(date)
	if err != nil {
		logger.Logger.Info("error get event by id in show event by day", zap.String("err", err.Error()))
		return nil, err
	}

	return events, nil
}

func (eu *EventUsecases) ShowEventByPeriod(start, end time.Time) ([]entities.Event, error) {
	events, err := eu.EventRepository.ShowByPeriod(start, end)
	if err != nil {
		logger.Logger.Info("error get events by period dates", zap.String("err", err.Error()))
		return nil, err
	}

	return events, nil
}

func (eu *EventUsecases) ShowEventByMonth(date time.Time) ([]entities.Event, error) {
	events, err := eu.EventRepository.ShowByMonth(date)
	if err != nil {
		logger.Logger.Info("error get events by month", zap.String("err", err.Error()))
		return nil, err
	}

	return events, nil
}

func (eu *EventUsecases) GetEventByID(id uint64) (entities.Event, error) {
	event, err := eu.EventRepository.GetByID(id)
	if err != nil {
		logger.Logger.Info("error get event by id in get event by id", zap.String("err", err.Error()))
		return entities.Event{}, err
	}

	return event, nil
}
