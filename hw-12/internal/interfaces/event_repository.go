package interfaces

import (
	"time"

	"gitlab.com/fed.nik.90/otus-go/hw-12/internal/entities"
)

type EventRepository interface {
	Create(event entities.Event) error
	Update(event entities.Event) error
	Delete(event entities.Event) error
	ShowByDay(date time.Time) ([]entities.Event, error)
	ShowByPeriod(start, end time.Time) ([]entities.Event, error)
	ShowByMonth(date time.Time) ([]entities.Event, error)
	Increment() uint64
	GetByID(id uint64) (entities.Event, error)
}
