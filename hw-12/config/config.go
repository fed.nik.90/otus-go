package config

import (
	"io/ioutil"
	"log"

	"gopkg.in/yaml.v2"
)

type Config struct {
	ListenIP   string `yaml:"ip"`
	ListenPort string `yaml:"http_port"`
	Network    string `yaml:"network"`
	LogLevel   string `yaml:"log_level"`
	LogFile    string `yaml:"log_file"`
}

func ParseConfig(path string) (*Config, error) {
	config := &Config{}
	confFile, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal(err.Error())
	}

	err = yaml.Unmarshal(confFile, config)
	if err != nil {
		log.Fatal(err.Error())
	}

	return config, nil
}
