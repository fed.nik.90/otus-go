package grpc

import (
	"context"
	"fmt"
	"gitlab.com/fed.nik.90/otus-go/hw-12/cmd"
	"gitlab.com/fed.nik.90/otus-go/hw-12/config"
	"gitlab.com/fed.nik.90/otus-go/hw-12/pkg/logger"
	"gitlab.com/fed.nik.90/otus-go/hw-12/proto"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"net"
	"os"
	"os/signal"
	"syscall"
)

var mem *cmd.Memory

type CalendarServer struct {
}

func Server(conf *config.Config) {
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, syscall.SIGINT, syscall.SIGTERM)

	mem = cmd.CreateMem()

	listenAddr := conf.ListenIP + ":" + conf.ListenPort
	logger.Logger.Info("Web server starting", zap.String("net", conf.Network), zap.String("ip", listenAddr))
	lis, err := net.Listen(conf.Network, listenAddr)
	if err != nil {
		logger.Logger.Fatal(err.Error())
	}

	grpcServer := grpc.NewServer()
	proto.RegisterCalendarServer(grpcServer, CalendarServer{})
	go func() {
		if err := grpcServer.Serve(lis); err != nil {
			logger.Logger.Fatal(err.Error())
		}
	}()
	<-interrupt
}

func (s CalendarServer) CreateEvent(ctx context.Context, msg *proto.CreateEventRequest) (*proto.CreateEventResponse, error) {
	fmt.Println("msg", msg)
	return nil, nil
}
