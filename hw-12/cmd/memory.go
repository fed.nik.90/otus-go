package cmd

import (
	"gitlab.com/fed.nik.90/otus-go/hw-12/internal/entities"
	"gitlab.com/fed.nik.90/otus-go/hw-12/internal/usecases"
	"time"
)

type Memory struct {
	action *usecases.EventUsecases
	memory *entities.EventRepo
}

func CreateMem() *Memory {
	er := new(entities.EventRepo)
	eu := new(usecases.EventUsecases)
	eu.EventRepository = er
	return &Memory{action: eu, memory: er}
}

func (m *Memory) AddEvent(title, desc string, start, end time.Time) (entities.Event, error) {
	event, err := m.action.AddEvent(title, desc, start, end)
	if err != nil {
		return entities.Event{}, err
	}
	return event, nil
}

func (m *Memory) UpdateEvent(id uint64, title, desc string, start, end time.Time) (entities.Event, error) {
	event, err := m.action.UpdateEvent(id, title, desc, start, end)
	if err != nil {
		return entities.Event{}, err
	}
	return event, nil
}

func (m *Memory) DeleteEvent(id uint64) error {
	err := m.action.DeleteEvent(id)
	if err != nil {
		return err
	}
	return nil
}

func (m *Memory) ShowEventByDay(day time.Time) ([]entities.Event, error) {
	events, err := m.action.ShowEventByDay(day)
	if err != nil {
		return []entities.Event{}, err
	}
	return events, nil
}

func (m *Memory) ShowEventByPeriod(start, end time.Time) ([]entities.Event, error) {
	events, err := m.action.ShowEventByPeriod(start, end)
	if err != nil {
		return []entities.Event{}, err
	}
	return events, nil
}

func (m *Memory) ShowEventByMonth(date time.Time) ([]entities.Event, error) {
	events, err := m.action.ShowEventByMonth(date)
	if err != nil {
		return []entities.Event{}, err
	}
	return events, nil
}

func (m *Memory) ShowMemory() *entities.EventRepo {
	return m.memory
}