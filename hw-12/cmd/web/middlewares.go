package web

import (
	"context"
	"net/http"
	"time"

	"gitlab.com/fed.nik.90/otus-go/hw-12/config"
	"gitlab.com/fed.nik.90/otus-go/hw-12/internal/helpers"
	"gitlab.com/fed.nik.90/otus-go/hw-12/pkg/logger"
)

func middlewareCreateEvent(h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		start, end, err := helpers.GetParseTimeFromString(r.FormValue("start"), r.FormValue("end"), config.FormatDate)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			logger.Logger.Fatal("not valid date time " + err.Error())
		}
		title, description, err := helpers.CheckString(r.FormValue("title"), r.FormValue("description"))
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			logger.Logger.Fatal("not valid string" + err.Error())
		}
		values := make(map[string]string)
		dates := make(map[string]time.Time)
		values["title"] = title
		values["description"] = description
		dates["start"] = start
		dates["end"] = end
		ctx := context.WithValue(r.Context(), "values", values)
		dctx := context.WithValue(ctx, "dates", dates)
		r = r.WithContext(dctx)
		h(w, r)
	}
}

func middlewareUpdateEvent(h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		id, err := helpers.GetParseIDFromString(r.FormValue("id"))
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			logger.Logger.Fatal("not valid id " + err.Error())
		}
		start, end, err := helpers.GetParseTimeFromString(r.FormValue("start"), r.FormValue("end"), config.FormatDate)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			logger.Logger.Fatal("not valid date time " + err.Error())
		}
		title, description, err := helpers.CheckString(r.FormValue("title"), r.FormValue("description"))
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			logger.Logger.Fatal("not valid string" + err.Error())
		}
		values := make(map[string]string)
		dates := make(map[string]time.Time)
		values["title"] = title
		values["description"] = description
		dates["start"] = start
		dates["end"] = end
		ctx := context.WithValue(r.Context(), "id", id)
		vctx := context.WithValue(ctx, "values", values)
		dctx := context.WithValue(vctx, "dates", dates)
		r = r.WithContext(dctx)
		h(w, r)
	}
}

func middlewareDeleteEvent(h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		id, err := helpers.GetParseIDFromString(r.FormValue("id"))
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			logger.Logger.Fatal("not valid id " + err.Error())
		}
		ctx := context.WithValue(r.Context(), "id", id)
		r = r.WithContext(ctx)
		h(w, r)
	}
}

func middlewareEventsForDay(h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		day, err := helpers.GetParseDateFromString(r.FormValue("date"), config.FormatDay)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			logger.Logger.Fatal("not valid date " + err.Error())
		}
		ctx := context.WithValue(r.Context(), "date", day)
		r = r.WithContext(ctx)
		h(w, r)
	}
}

func middlewareEventsForWeek(h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		start, end, err := helpers.GetParseTimeFromString(r.FormValue("start"), r.FormValue("end"), config.FormatDay)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			logger.Logger.Fatal("not valid date time " + err.Error())
		}
		dates := make(map[string]time.Time)
		dates["start"] = start
		dates["end"] = end
		ctx := context.WithValue(r.Context(), "dates", dates)
		r = r.WithContext(ctx)
		h(w, r)
	}
}

func middlewareEventsForMonth(h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		date, err := helpers.GetParseDateFromString(r.FormValue("date"), config.FormatMonth)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			logger.Logger.Fatal("not valid date " + err.Error())
		}
		ctx := context.WithValue(r.Context(), "date", date)
		r = r.WithContext(ctx)
		h(w, r)
	}
}
