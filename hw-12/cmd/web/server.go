package web

import (
	"fmt"
	"gitlab.com/fed.nik.90/otus-go/hw-12/cmd"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"go.uber.org/zap"

	"github.com/gorilla/mux"
	"gitlab.com/fed.nik.90/otus-go/hw-12/config"
	"gitlab.com/fed.nik.90/otus-go/hw-12/pkg/logger"
)

var mem *cmd.Memory

func Web(conf *config.Config) {
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, syscall.SIGINT, syscall.SIGTERM)

	mem = cmd.CreateMem()

	router := mux.NewRouter()
	routers(router)

	listenAddr := conf.ListenIP + ":" + conf.ListenPort
	logger.Logger.Info("Web server starting", zap.String("ip", listenAddr))
	webServer := &http.Server{Addr: listenAddr, Handler: router}
	go func() {
		if err := webServer.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			logger.Logger.Fatal(err.Error())
		}
	}()
	<-interrupt

	fmt.Println("memory*", *mem.ShowMemory())
}
