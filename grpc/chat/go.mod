module chat

go 1.12

require (
	github.com/golang/protobuf v1.4.2
	google.golang.org/grpc v1.25.1
)
