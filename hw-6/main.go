package main

import (
	"flag"
	"fmt"

	fc "gitlab.com/fed.nik.90/otus-go/hw-6/filecopy"
)

var (
	from, to      string
	limit, offset int64
)

func init() {
	flag.StringVar(&from, "from", "", "file to read from")
	flag.StringVar(&to, "to", "", "file to write to")
	flag.Int64Var(&limit, "limit", 0, "limit of bytes to copy")
	flag.Int64Var(&offset, "offset", 0, "offset in input file")
}

func main() {
	flag.Parse()

	if from == "" || to == "" {
		flag.PrintDefaults()
		return
	}

	err := fc.Copy(from, to, offset, limit)
	if err != nil {
		fmt.Println(err)
	}
}
