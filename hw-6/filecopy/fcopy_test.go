package filecopy

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestCopy(t *testing.T) {
	t.Run("TestCopySuccess", func(t *testing.T) {
		casesSuccess := []struct {
			from, to                 string
			offset, limit, size, res int64
		}{
			{
				from:   "../test.txt",
				to:     "../test_out.txt",
				offset: 0,
				limit:  0,
				size:   34,
				res:    34,
			},
			{
				from:   "../test.txt",
				to:     "../test_out.txt",
				offset: 10,
				limit:  0,
				size:   34,
				res:    24,
			},
			{
				from:   "../test.txt",
				to:     "../test_out.txt",
				offset: 0,
				limit:  10,
				size:   34,
				res:    10,
			},
			{
				from:   "../test.txt",
				to:     "../test_out.txt",
				offset: 10,
				limit:  10,
				size:   34,
				res:    10,
			},
		}
		t.Run("if copy  success", func(t *testing.T) {
			for _, v := range casesSuccess {
				err := Copy(v.from, v.to, v.offset, v.limit)
				if err != nil {
					t.Errorf("TestCopySuccess(), err while start function")
				}
				in, err := os.Open(v.from)
				if err != nil {
					t.Errorf("TestCopySuccess(), error while open in file")
				}
				ins, err := in.Stat()
				if err != nil {
					t.Errorf("TestCopySuccess(), error while get in file statistic")
				}
				if ins.Size() != v.size {
					t.Errorf("TestCopySuccess(), size in file is not equal size == %d, file size == %d", ins.Size(), v.size)
				}
				out, err := os.Open(v.to)
				if err != nil {
					t.Errorf("TestCopySuccess(), error while open out file")
				}
				outs, err := out.Stat()
				if err != nil {
					t.Errorf("TestCopySuccess(), error while get out file statistic")
				}
				require.Equal(t, outs.Size(), v.res, "TestCopySuccess(), size is not equal size == %d, limit == %d, offset == %d", outs.Size(), v.limit, v.offset)
			}
		})
	})
	t.Run("TestCopyUnEqual", func(t *testing.T) {
		casesNotEqual := []struct {
			from, to                 string
			offset, limit, size, res int64
		}{
			{
				from:   "../test.txt",
				to:     "../test_out.txt",
				offset: 0,
				limit:  0,
				size:   34,
				res:    0,
			},
			{
				from:   "../test.txt",
				to:     "../test_out.txt",
				offset: 10,
				limit:  0,
				size:   34,
				res:    34,
			},
			{
				from:   "../test.txt",
				to:     "../test_out.txt",
				offset: 0,
				limit:  10,
				size:   34,
				res:    34,
			},
			{
				from:   "../test.txt",
				to:     "../test_out.txt",
				offset: 10,
				limit:  10,
				size:   34,
				res:    34,
			},
		}
		t.Run("if copy  notequal", func(t *testing.T) {
			for _, v := range casesNotEqual {
				err := Copy(v.from, v.to, v.offset, v.limit)
				if err != nil {
					t.Errorf("TestCopyUnEqual(), err while start function")
				}
				in, err := os.Open(v.from)
				if err != nil {
					t.Errorf("TestCopyUnEqual(), error while open in file")
				}
				ins, err := in.Stat()
				if err != nil {
					t.Errorf("TestCopyUnEqual(), error while get in file statistic")
				}
				if ins.Size() != v.size {
					t.Errorf("TestCopyUnEqual(), size in file is not equal size == %d, file size == %d", ins.Size(), v.size)
				}
				out, err := os.Open(v.to)
				if err != nil {
					t.Errorf("TestCopyUnEqual(), error while open out file")
				}
				outs, err := out.Stat()
				if err != nil {
					t.Errorf("TestCopyUnEqual(), error while get out file statistic")
				}
				require.NotEqual(t, outs.Size(), v.res, "TestCopyUnEqual(), size is not equal size == %d, limit == %d, offset == %d", outs.Size(), v.limit, v.offset)
			}
		})
	})
	t.Run("TestCopyError", func(t *testing.T) {
		casesError := []struct {
			from, to                 string
			offset, limit, size, res int64
		}{
			{
				from:   "../somefile.txt",
				to:     "../somefile_out.txt",
				offset: 0,
				limit:  0,
				size:   34,
				res:    34,
			},
			{
				from:   "../test.txt",
				to:     "../test_out.txt",
				offset: 1000,
				limit:  20000,
				size:   34,
				res:    100,
			},
			{
				from:   "../filecopy",
				to:     "../filecopy_out",
				offset: 0,
				limit:  0,
				size:   34,
				res:    34,
			},
			{
				from:   "",
				to:     "",
				offset: 0,
				limit:  0,
				size:   34,
				res:    34,
			},
			{
				from:   "../testnil.txt",
				to:     "../testnil_out.txt",
				offset: 100,
				limit:  0,
				size:   0,
				res:    0,
			},
			{
				from:   "../testnil.txt",
				to:     "../testnil",
				offset: 0,
				limit:  120,
				size:   0,
				res:    0,
			},
		}
		for _, v := range casesError {
			t.Run("if get err from Copy()  err", func(t *testing.T) {
				err := Copy(v.from, v.to, v.offset, v.limit)
				require.Error(t, err)
			})
		}
	})
}
