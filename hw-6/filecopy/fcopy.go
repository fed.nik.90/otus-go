package filecopy

import (
	"errors"
	"io"
	"math"
	"os"

	"github.com/cheggaaa/pb/v3"
)

const MB = 1024 * 1024 * 10

var (
	ErrUnsupported    = errors.New("unsupported file")
	ErrOffsetFileSize = errors.New("offset exceeds file size")
)

func Copy(from, to string, offset, limit int64) error {
	fileStat, err := os.Stat(from)
	if err != nil {
		return err
	}
	if !fileStat.Mode().IsRegular() {
		return ErrUnsupported
	}
	fileSize := fileStat.Size()
	if fileSize <= offset {
		return ErrOffsetFileSize
	}
	if limit == 0 {
		limit = fileSize - offset
	}
	in, err := os.Open(from)
	if err != nil {
		return err
	}
	out, err := os.Create(to)
	if err != nil {
		return err
	}
	defer func() {
		_ = in.Close()
		_ = out.Close()
	}()
	_, err = in.Seek(offset, io.SeekStart)
	if err != nil {
		return err
	}

	barMax := limit
	bar := pb.Full.Start64(barMax)
	barReader := bar.NewProxyReader(in)
	buf := make([]byte, int(math.Min(float64(MB), float64(limit))))
	for {
		n, err := barReader.Read(buf)
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}
		if _, err := out.Write(buf[:n]); err != nil {
			return err
		}
		limit -= int64(n)
		if limit <= 0 {
			break
		}
	}
	bar.SetCurrent(barMax)
	bar.Finish()
	return nil
}
